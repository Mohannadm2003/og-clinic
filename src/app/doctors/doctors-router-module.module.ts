import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClinicMainPageComponent } from '../pages/clinic-main-page/clinic-main-page.component';
import { MyClinicComponent } from '../pages/my-clinic/my-clinic.component';
import { MedicalStorageComponent } from '../pages/medical-storage/medical-storage.component';
import { MedicalReferralNetworkComponent } from '../pages/medical-referral-network/medical-referral-network.component';
import { PatientViewComponent } from '../pages/patient-view/patient-view.component';
import { IcsiComponent } from '../pages/icsi/icsi.component';
import { EditDoctorComponent } from '../pages/edit-doctor/edit-doctor.component';
import { AllDoctorsComponent } from '../pages/all-doctors/all-doctors.component';
import { RouterModule } from '@angular/router';

const routes = [
	{
		path: '',
		component: ClinicMainPageComponent,
		children: [
			{ path: 'patient', component: MyClinicComponent },
			{ path: 'doctors', component: AllDoctorsComponent },
			{ path: 'patient/:id', component: PatientViewComponent },
			{ path: 'icsi/:id', component: IcsiComponent },
			{ path: 'editProfile/:id', component: EditDoctorComponent },
      {
        path: 'calendar',
        loadChildren: () =>
          import('../Shared/UI/calendar/calendar.module').then(
            (m) => m.CalendarStandalonModule
          ),
      },
			{ path: 'storage', component: MedicalStorageComponent },
			{ path: 'network', component: MedicalReferralNetworkComponent },
		],
	},
];
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class DoctorsRouterModuleModule {}
