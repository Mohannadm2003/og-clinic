import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import {  NgbModalModule,NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { FlatpickrDefaults, FlatpickrModule } from 'angularx-flatpickr';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DifferenceinweeksPipe } from 'src/app/Shared/pipes/differenceinweeks.pipe';

import { DoctorSharedModule } from '../doctor-shared/doctor-shared.module';
import { AllDoctorsComponent } from '../pages/all-doctors/all-doctors.component';
import { ClinicMainPageComponent } from '../pages/clinic-main-page/clinic-main-page.component';
import { EditDoctorComponent } from '../pages/edit-doctor/edit-doctor.component';
import { IcsiComponent } from '../pages/icsi/icsi.component';
import { MedicalReferralNetworkComponent } from '../pages/medical-referral-network/medical-referral-network.component';
import { MedicalStorageComponent } from '../pages/medical-storage/medical-storage.component';
import { MyClinicComponent } from '../pages/my-clinic/my-clinic.component';
import { PatientViewComponent } from '../pages/patient-view/patient-view.component';
import { SharedModule } from '../Shared/shared-modules.module';
import { ToolbarComponent } from '../Shared/UI/toolbar/toolbar.component';
import { DoctorsRouterModuleModule } from './doctors-router-module.module';

@NgModule({
  declarations: [
    MyClinicComponent,
    AllDoctorsComponent,
    PatientViewComponent,
    IcsiComponent,
    EditDoctorComponent,
    MedicalStorageComponent,
    MedicalReferralNetworkComponent,
    ToolbarComponent,
    ClinicMainPageComponent,
    DifferenceinweeksPipe,
  ],
  imports: [
    CommonModule,
    SharedModule,
    DoctorSharedModule,
    DoctorsRouterModuleModule,
    NgbDropdownModule,
    NgbModalModule,
    MatExpansionModule,
    FlatpickrModule,
    NgMultiSelectDropDownModule.forRoot(),
  ],

  providers: [FlatpickrDefaults],
})
export class DoctorsModule {}
