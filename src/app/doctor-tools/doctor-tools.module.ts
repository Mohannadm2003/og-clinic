import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorBlogComponent } from '../pages/doctor-blog/doctor-blog.component';
import { DoctorPostComponent } from '../pages/doctor-post/doctor-post.component';
import { ClinicLoginComponent } from '../pages/clinic-login/clinic-login.component';
import { DoctorToolsRouterModule } from './doctor-tools-router.module';
import { SharedModule } from '../Shared/shared-modules.module';
import { MatExpansionModule } from '@angular/material/expansion';

import { DoctorToolsHomeComponent } from '../pages/doctor-tools-home/doctor-tools-home.component';
import { DrugCatsComponent } from '../pages/drug-cats/drug-cats.component';
import { SharedUiModule } from '../Shared/shared-ui.module';

@NgModule({
	declarations: [
		DoctorBlogComponent,
		DoctorPostComponent,
		ClinicLoginComponent,
		DoctorToolsHomeComponent,
		DrugCatsComponent,
	],
	imports: [
		CommonModule,
		DoctorToolsRouterModule,
		MatExpansionModule,
		SharedModule,
    SharedUiModule
	],
})
export class DoctorToolsModule {}
