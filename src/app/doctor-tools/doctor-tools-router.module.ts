import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorBlogComponent } from '../pages/doctor-blog/doctor-blog.component';
import { DoctorPostComponent } from '../pages/doctor-post/doctor-post.component';
import { ClinicLoginComponent } from '../pages/clinic-login/clinic-login.component';

import { DoctorToolGuard } from '../Shared/services/guard/doctor/doctor-tool.guard';
import { RouterModule } from '@angular/router';
import { DoctorToolsHomeComponent } from '../pages/doctor-tools-home/doctor-tools-home.component';
import { DrugCatsComponent } from '../pages/drug-cats/drug-cats.component';
const routes = [
	{
		path: 'home',
		component: DoctorToolsHomeComponent,
		// canActivate: [DoctorToolGuard],
	},
  {
		path: 'drug',
		component: DrugCatsComponent,
		// canActivate: [DoctorToolGuard],
	},
  {
		path: 'blog',
		component: DoctorBlogComponent,
		// canActivate: [DoctorToolGuard],
	},

	{
		path: 'blog/:id',
		component: DoctorPostComponent,
		// canActivate: [DoctorToolGuard],
	},
];
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class DoctorToolsRouterModule {}
