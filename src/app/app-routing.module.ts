import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';


import { MyclinicGuard } from './Shared/guard/myclinic.guard';
import { DoctorToolGuard } from './Shared/services/guard/doctor/doctor-tool.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
	{ path: 'home', component: HomeComponent },

  {
    path: 'auth',
    loadChildren: () =>
      import('./doctors-auth/doctors-auth.module').then(
        (m) => m.DoctorsAuthModule
      ),
  },

  {
    path: '',
    loadChildren: () =>
      import('./patients/patients.module').then((m) => m.PatientsModule),
  },

  {
    path: 'doctors',
    loadChildren: () =>
      import('./doctor-tools/doctor-tools.module').then(
        (m) => m.DoctorToolsModule
      ),
    canActivateChild: [DoctorToolGuard],
  },

  {
    path: 'myclinic',
    loadChildren: () =>
      import('./doctors/doctors.module').then((m) => m.DoctorsModule),
    canActivateChild: [MyclinicGuard],
  },

  {
    path: '',
    loadChildren: () =>
      import('./admin/admin.module').then((m) => m.AdminModule),
  },

  { path: '**', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
