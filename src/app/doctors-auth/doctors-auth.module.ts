import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorAuthComponent } from '../pages/doctor-auth/doctor-auth.component';
import { ForgetPasswordComponent } from '../pages/forget-password/forget-password.component';
import { ResetPasswordComponent } from '../pages/reset-password/reset-password.component';
import { SharedModule } from '../Shared/shared-modules.module';
import { ClinicLoginComponent } from '../pages/clinic-login/clinic-login.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'clinicLogin', component: ClinicLoginComponent },
  { path: 'doctools', component: DoctorAuthComponent },
  { path: 'forgetPassword', component: ForgetPasswordComponent },
  { path: 'resetPassword', component: ResetPasswordComponent },
];

@NgModule({
  declarations: [
    DoctorAuthComponent,
    ForgetPasswordComponent,
    ResetPasswordComponent,
  ],
  imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DoctorsAuthModule {}
