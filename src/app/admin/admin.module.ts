import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

import "/node_modules/froala-editor/js/froala_editor.pkgd.min.js"

import { CreateBlogComponent } from '../pages/blogs/create-blog/create-blog.component';
import { BlogsComponent } from '../pages/blogs/blogs/blogs.component';
import { SharedModule } from '../Shared/shared-modules.module';
import { DoctorSharedModule } from '../doctor-shared/doctor-shared.module';
// import { ToolbarComponent } from '../Shared/UI/toolbar/toolbar.component';



const routes = [
	{ path: 'myclinic/posts', component: BlogsComponent },
	{ path: 'myclinic/posts/post', component: CreateBlogComponent },
	{ path: 'myclinic/posts/post/:id', component: CreateBlogComponent },
];

@NgModule({
	declarations: [BlogsComponent, CreateBlogComponent,],
	imports: [
		CommonModule,
		SharedModule,
		FroalaEditorModule.forRoot(),
		FroalaViewModule.forRoot(),
    DoctorSharedModule,
		RouterModule.forChild(routes),
	],
})
export class AdminModule {}
