import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { res } from 'src/app/Shared/models/res.model';
import { DoctorService } from 'src/app/Shared/services/doctor/doctor.service';
import { faMap } from '@fortawesome/free-regular-svg-icons';
import { faMapMarked, faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { faWhatsapp, faFacebook } from '@fortawesome/free-brands-svg-icons';
@Component({
	selector: 'app-doctors-cards',
	templateUrl: './doctors-cards.component.html',
	styleUrls: ['./doctors-cards.component.css'],
})
export class DoctorsCardsComponent implements OnInit {
	result: any[] = [];
	doctor: any;
	countries;
	countryName;
	cityName;
	faMapMarked = faMapMarked;
	faWhatsapp = faWhatsapp;
	faFacebook = faFacebook;
	faPhoneAlt = faPhoneAlt;
	cites;
	pager = {
		pageSize: 12,
		length: 0,
	};
	isLoading = true;
	constructor(
		private modalService: NgbModal,
		private doctorServices: DoctorService,
		private _snackBar: MatSnackBar
	) {}

	ngOnInit(): void {
		this.doctorServices.getCountries().subscribe((res: res) => {
			this.countries = res.sentObject;
		});
		this.getAllDoctors();
	}

	open(content: any, i) {
		this.doctorServices
			.getDoctor(this.result[i]._id)
			.subscribe((res: res) => {
				this.doctor = res.sentObject;
			});
		this.modalService.open(content, {
			centered: true,
			size: 'l',
			backdrop: true,
		});
	}

	getAllDoctors() {
		this.doctorServices.getDoctors(12, 1).subscribe(
			(res: res) => {
				this.result = res.sentObject;
				this.pager.length = res.count;
				this.isLoading = false;
			},
			(err) => {
				this._snackBar.open(err.error.message, 'close');
				this.isLoading = false;
			}
		);
	}

	handlePage(event: any) {
		this.doctorServices.getDoctors(12, event.pageIndex + 1).subscribe(
			(res: res) => {
				this.pager.pageSize = 12;
				this.pager.length = res.count;
				this.result = res.sentObject;
				this.isLoading = false;
			},
			(err) => {
				this._snackBar.open(err.error.message, 'close');
				this.isLoading = false;
			}
		);
	}

	showCity(event) {
		const value = event.target.value;
		this.countryName = value;
		const isoCode = value.split(',')[1];
		this.doctorServices.getCitiesOFCountry(isoCode).subscribe((res: res) => {
			this.cites = res.sentObject;
		});
		this.filter(this.countryName);
	}

	filterByCity(event) {
		this.cityName = event.target.value;
		this.filter(this.countryName, this.cityName);
	}

	filterByName(event) {
		this.filter(this.countryName, this.cityName, event.target.value);
	}

	filter(country, city?, name?) {
		this.doctorServices.filterDoctor(country, city, name).subscribe(
			(res: res) => {
				this.result = [...res.sentObject];
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}
}
