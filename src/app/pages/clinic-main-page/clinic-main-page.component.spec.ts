import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicMainPageComponent } from './clinic-main-page.component';

describe('ClinicMainPageComponent', () => {
  let component: ClinicMainPageComponent;
  let fixture: ComponentFixture<ClinicMainPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClinicMainPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicMainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
