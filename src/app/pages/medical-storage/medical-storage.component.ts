import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { CookieService } from 'ngx-cookie-service';
import { res } from 'src/app/Shared/models/res.model';
import { DoctorService } from 'src/app/Shared/services/doctor/doctor.service';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
	selector: 'app-medical-storage',
	templateUrl: './medical-storage.component.html',
	styleUrls: ['./medical-storage.component.css'],
})
export class MedicalStorageComponent implements OnInit {
	ColumnMode = ColumnMode;
	faTrash = faTrash;
	editing = {};
	medicalStorage;
	medicalStorageForm: FormGroup;
	modalRef: NgbModalRef;
	constructor(
		private cookieService: CookieService,
		private fb: FormBuilder,
		private modalService: NgbModal,
		private doctorService: DoctorService,
		private _snackBar: MatSnackBar
	) {}

	ngOnInit(): void {
		this.initForm();
		this.doctorService.getMedicalStorage().subscribe((res: res) => {
			this.medicalStorage = res.sentObject;
		});
	}

	updateValue(event: Event, cell, rowIndex) {
		const value = (<HTMLInputElement>event.target).value;
		const id = this.medicalStorage[rowIndex]._id;
		this.doctorService.updateMedicalStorage(id, { [cell]: value }).subscribe(
			(res) => {
				this.medicalStorage[rowIndex][cell] = value;
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
		this.editing[rowIndex + '-' + cell] = false;
	}

	addToMedicalSupply() {
		this.doctorService
			.addToMedicalStorage(this.medicalStorageForm.value)
			.subscribe(
				(res: res) => {
					this.medicalStorage = [...res.sentObject];
          this.modalRef.close();
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);
	}

	initForm() {
		this.medicalStorageForm = this.fb.group({
			medicalSupply: [''],
			presentOrNot: [''],
			number: [''],
			lastStoking: [''],
		});
	}

	openModal(modal) {
		this.modalRef = this.modalService.open(modal, {
			size: 'xl',
			centered: true,
		});
		this.modalRef.result.then(
			(result) => {
				this.medicalStorageForm.reset();
			},
			(reason) => {
				this.medicalStorageForm.reset();
			}
		);
	}

	deleteMedicalStorage(id, index) {
		this.doctorService.deleteMedicalStorage(id).subscribe(
			(res) => {
				this.medicalStorage = this.medicalStorage.filter((m, i) => {
					return i !== index;
				});

				// const doctor: any = JSON.parse(this.cookieService.get('userDoc'));
				// doctor.medicalSupplyStorage = [...this.medicalStorage];
				// this.cookieService.set('s', JSON.stringify(doctor));
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}
}
