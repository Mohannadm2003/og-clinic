import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalStorageComponent } from './medical-storage.component';

describe('MedicalStorageComponent', () => {
  let component: MedicalStorageComponent;
  let fixture: ComponentFixture<MedicalStorageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalStorageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
