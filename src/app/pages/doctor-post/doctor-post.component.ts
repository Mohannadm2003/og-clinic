import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

import { res } from 'src/app/Shared/models/res.model';
import { PostService } from 'src/app/Shared/services/post/post.service';
import { Post } from '../../Shared/models/post.model';
@Component({
	selector: 'app-doctor-post',
	templateUrl: './doctor-post.component.html',
	styleUrls: ['./doctor-post.component.css'],
})
export class DoctorPostComponent implements OnInit {
	isdbClicked: boolean = false;
	id: string;
	post: Post;
	editing: any[] = [];
	myPrescription: any;

	constructor(
		private readonly postService: PostService,
		private readonly _snackBar: MatSnackBar,
		private readonly route: ActivatedRoute,
		private readonly sanitizer: DomSanitizer
	) {}

	ngOnInit(): void {
		this.id = this.route.snapshot.params.id;
		if (typeof this.id !== undefined) {
			this.postService.getToolsPost(this.id).subscribe(
				(res: res) => {
					this.post = res.sentObject.post;
					// this.myPrescription = res.sentObject.myPrescription;
					// this.addingFromValue();
					// this.post.img = this.sanitizer.bypassSecurityTrustUrl(
					//   'http://localhost:3000' + this.post.img
					// );
					this.post.body = this.sanitizer.bypassSecurityTrustHtml(
						this.post.body as string
					);
				},
				(error) => this._snackBar.open(error.error.message)
			);
		}
	}

	editTextArea(i, id) {
		this.editing[i] = true;
	}
	save(event: any,) {
		// this.editing[i] = false;
		const value = event.target.value;
		if (this.post.myPrescription && this.post.myPrescription.value) {
			this.postService
				.updatePrescriptions(this.post.myPrescription._id, {
					value: value,
				})
				.subscribe(
					(res: res) => {
						this.post.myPrescription.value = value;
						// this.myPrescription[i].value = event.target.value;
						// this.post.drugForms[i].value = event.target.value;
					},
					(err) => {
						this._snackBar.open(err.error.message, 'close');
					}
				);
		} else {
			this.postService
				.addPrescriptions(this.post.myPrescription._id, {
					value: value,
					postId: this.id,
				})
				.subscribe(
					(res: res) => {
						this.post.myPrescription.value = value;
						this.post.myPrescription._id = res.sentObject.id;

						// this.post.drugForms[i].value = value;
					},
					(err) => {
						this._snackBar.open(err.error.message, 'close');
					}
				);
		}
	}

	// addingFromValue() {
	//   this.post.drugForms.forEach((form, i) => {
	//     for (let p of this.myPrescription) {
	//       if (form._id === p.formId) {
	//         this.post.drugForms[i].value = p.value;
	//       }
	//     }
	//   });
	// }
}
