import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { res } from 'src/app/Shared/models/res.model';

import { DoctorService } from 'src/app/Shared/services/doctor/doctor.service';

@Component({
	selector: 'app-doctor-auth',
	templateUrl: './doctor-auth.component.html',
	styleUrls: ['./doctor-auth.component.css'],

})
export class DoctorAuthComponent implements OnInit {
	isLogin: boolean = true;
	logInForm: FormGroup;
	signUpForm: FormGroup;
	countries: any[];
	cities: any[];

	constructor(
		private readonly route: ActivatedRoute,
		private readonly doctorServices: DoctorService,
		private readonly _snackBar: MatSnackBar,
		private readonly router: Router
	) {}

	ngOnInit(): void {
    this.initLoginForm();
		this.initSignupForm();
		this.isTheFormLogin();
    const isAuth = this.doctorServices.isEnabled();
		if (isAuth) {
      this.router.navigate(['/', 'doctors', 'home']);
			return
		}
		this.doctorServices.getCountries().subscribe((res: res) => {
			this.countries = res.sentObject;
		});

	}

	isTheFormLogin() {
		this.route.queryParams.subscribe((p) => {
			if (Object.keys(p).length === 0) return;
			this.isLogin = JSON.parse(p.login);
		});
	}

	initLoginForm() {
		this.logInForm = new FormGroup({
			email: new FormControl('', [Validators.required, Validators.email]),
			password: new FormControl('', [Validators.required]),
		});
	}

	initSignupForm() {
		this.signUpForm = new FormGroup({
			firstName: new FormControl('', [Validators.required]),
			lastName: new FormControl('', [Validators.required]),
			address: new FormControl('', [Validators.required]),
			email: new FormControl('', [Validators.required, Validators.email]),
			password: new FormControl('', [Validators.required,Validators.minLength(6)]),
			country: new FormControl(''),
			city: new FormControl(''),
			gender: new FormControl('', [Validators.required]),
			speciality: new FormControl('', [Validators.required]),
		});
	}

	login() {
		this.doctorServices.logIn(this.logInForm.value).subscribe(
			(res: { sentObject; token; message ;role; enabled; inMyClinic}) => {
				this.doctorServices.saveAuthData(
					res.token,
					Object.assign(res.sentObject),
					res.role,
					res.enabled,
					res.inMyClinic
				);
				this.doctorServices.isAuth = true;
				this.doctorServices.isAuthlistener.next(true);
				this.router.navigate(['/', 'doctors', 'home']);
			},
			(err) => {
				this._snackBar.open(err.error.message, 'close');
			}
		);
	}

	signup() {
		this.doctorServices.signUp(this.signUpForm.value).subscribe(
			(res) => {},
			(err) => {
				this._snackBar.open(err.error.message, 'close');
			}
		);
	}
	getCities(value: string) {
		let code = value.split(',')[0];
		let counName = value.split(',')[1];
		this.doctorServices.getCitiesOFCountry(code).subscribe((res: res) => {
			this.cities = res.sentObject;
		});
		this.signUpForm.get('country').setValue(counName);
	}
	show() {}
}
