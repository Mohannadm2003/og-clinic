import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { res } from 'src/app/Shared/models/res.model';
import { PatientService } from 'src/app/Shared/services/patients/patient.service';

@Component({
	selector: 'app-record',
	templateUrl: './record.component.html',
	styleUrls: ['./record.component.css'],
})
export class RecordComponent implements OnInit {
	record;
	id = this.route.snapshot.paramMap.get('id');
	title = this.route.snapshot.queryParamMap.get('title');
	type = this.route.snapshot.queryParamMap.get('type');

  imageInModal=''
	constructor(
		private route: ActivatedRoute,
		private patientService: PatientService,
		private modalSevice: NgbModal
	) {}

	ngOnInit(): void {
		this.patientService
			.getRecord(this.id, this.title, this.type)
			.subscribe((res: res) => {
				this.record = res.sentObject;
			});
	}

	openModal(modal,img) {
    this.imageInModal = img;
		this.modalSevice.open(modal, { centered: true, size: 'xl' });
	}
}
