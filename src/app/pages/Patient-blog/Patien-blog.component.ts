import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DomSanitizer } from '@angular/platform-browser';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Router,
} from '@angular/router';
import { postType } from 'src/app/Shared/enum/types.enum';
import { res } from 'src/app/Shared/models/res.model';
import { PostService } from 'src/app/Shared/services/post/post.service';

@Component({
  selector: 'app-Patient-blog',
  templateUrl: './Patient-blog.component.html',
  styleUrls: ['./Patient-blog.component.css'],
})
export class PatientBlogComponent implements OnInit {
  posts: any[];
  pager = {
    pageSize: 0,
    length: 0,
    page: 1,
  };
  isLoading = true;
  cat;
  queryParams;
  constructor(
    private postService: PostService,
    private router: Router,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar // private sanitizer: DomSanitizer,
  ) {}

  ngOnInit(): void {
    this.cat = this.route.snapshot.queryParamMap.get('cat');
    this.route.queryParams.subscribe((p) => {
      this.queryParams = JSON.parse(JSON.stringify(p));
      this.pager.page = p.page;
    });
    this.postService
      .getPost('', 12, this.pager.page, postType.patient, this.cat)
      .subscribe(
        (res: res) => {
          this.pager.pageSize = 12;
          this.pager.length = res.count;
          this.posts = res.sentObject;
          this.isLoading = false;
          // this.posts.forEach((p) => {
          // 	p.img = this.sanitizer.bypassSecurityTrustUrl(
          // 		'http://localhost:3000' + p.img
          // 	);
          // });
        },
        (err) => {
          this._snackBar.open(err.error.message, 'close');
          this.isLoading = false;
        }
      );
  }

  handlePage(event: PageEvent) {
    this.cat = this.route.snapshot.queryParamMap.get('cat');

    this.postService
      .getPost('', 12, event.pageIndex + 1, postType.patient, this.cat)
      .subscribe(
        (res: res) => {
          this.pager.pageSize = 12;
          this.pager.length = res.count;
          this.pager.page = event.pageIndex + 1;
          this.queryParams.page = event.pageIndex + 1;
          this.posts = res.sentObject;
          this.isLoading = false;
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: this.queryParams,
            queryParamsHandling: 'merge', // remove to replace all query params by provided
          });
          // this.posts.forEach((p) => {
          // 	p.img = this.sanitizer.bypassSecurityTrustUrl(
          // 		'http://localhost:3000' + p.img
          // 	);
          // });
        },
        (err) => {
          this._snackBar.open(err.error.message, 'close');
          this.isLoading = false;
        }
      );
  }
}
