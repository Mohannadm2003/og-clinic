import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  details = navigator.userAgent;

  regexp = /android|iphone|kindle|ipad/i;
  
  isMobileDevice;
  constructor() {
    this.isMobileDevice = this.regexp.test(this.details);
  }

  ngOnInit(): void {
  }

}
