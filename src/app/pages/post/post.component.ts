import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { res } from 'src/app/Shared/models/res.model';
import { PostService } from 'src/app/Shared/services/post/post.service';

@Component({
	selector: 'app-post',
	templateUrl: './post.component.html',
	styleUrls: ['./post.component.css'],
})
export class PostComponent implements OnInit {
	post: any;
	id;
	constructor(
		private route: ActivatedRoute,
		private postService: PostService,
		private sanitizer: DomSanitizer,
		private _snackBar: MatSnackBar
	) {}

	ngOnInit(): void {
		this.id = this.route.snapshot.params.id;
		if (typeof this.id !== undefined) {
			this.postService.getPost(this.id).subscribe(
				(res: res) => {
					this.post = res.sentObject;
					this.post.content = this.sanitizer.bypassSecurityTrustHtml(
						this.post.body
					);
				},
				(error) => this._snackBar.open(error.error.message)
			);
		}
	}
}
