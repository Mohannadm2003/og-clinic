import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { postType } from 'src/app/Shared/enum/types.enum';
import { res } from 'src/app/Shared/models/res.model';
import { PostService } from 'src/app/Shared/services/post/post.service';

@Component({
	selector: 'app-nurse-blog',
	templateUrl: './nurse-blog.component.html',
	styleUrls: ['./nurse-blog.component.css'],
})
export class NurseBlogComponent implements OnInit {
	posts: any[] = [];
	pager = {
		pageSize: 0,
		length: 0,
	};
	isLoading = true;
	constructor(
		private postService: PostService,
		private _snackBar: MatSnackBar
	) {}

	ngOnInit(): void {
		this.postService.getPost('', 12, 1, postType.nursing).subscribe(
			(res: res) => {
				this.pager.pageSize = 12;
				this.pager.length = res.count;
				this.posts = [...res.sentObject];
				this.isLoading = false;
			},
			(err) => {
				this._snackBar.open(err.error.message, 'close');
				this.isLoading = false;
			}
		);
	}

	handlePage(event: PageEvent) {
		this.postService
			.getPost('', 12, event.pageIndex + 1, postType.nursing)
			.subscribe(
				(res: res) => {
					this.pager.pageSize = 12;
					this.pager.length = res.count;
					this.posts = res.sentObject;
					setTimeout(() => {
						this.isLoading = false;
					}, 5000);

					// this.posts.forEach((p) => {
					// 	p.img = this.sanitizer.bypassSecurityTrustUrl(
					// 		'http://localhost:3000' + p.img
					// 	);
					// });
				},
				(err) => {
					this._snackBar.open(err.error.message, 'close');
					this.isLoading = false;
				}
			);
	}
}
