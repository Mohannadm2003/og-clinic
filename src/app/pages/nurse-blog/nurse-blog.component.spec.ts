import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NurseBlogComponent } from './nurse-blog.component';

describe('NurseBlogComponent', () => {
  let component: NurseBlogComponent;
  let fixture: ComponentFixture<NurseBlogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NurseBlogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NurseBlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
