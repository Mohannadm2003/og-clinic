import { Component, OnInit } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { PatientService } from 'src/app/Shared/services/patients/patient.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { res } from '../../Shared/models/res.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import add from 'date-fns/add';
import { differenceInCalendarWeeks } from 'date-fns/esm';
@Component({
	selector: 'app-patient-view',
	templateUrl: './patient-view.component.html',
	styleUrls: ['./patient-view.component.css'],
})
export class PatientViewComponent implements OnInit {
	ColumnMode = ColumnMode;
	ReasonsList = [
		'Antenatal care',
		'infertility',
		'bleeding',
		'infection',
		'pelvic pain',
		'swelling',
		'contraception',
		'other',
	];
	complaintList = [
		'infertility',
		'Prolapse',
		'bleeding',
		'infection',
		'pelvic pain',
		'swelling',
		'Dyspareunia',
		'Urinary complaints',
		'Gastrointestinal complaints',
		'Respiratory complaints',
		'Urological complaint',
		'Vascular complaint',
		'Musculoskeletal complaints',
		'Cutaneous complaints',
		'precocious puberty',
		'Delayed Puberty',
		'Menopause',
		'Bartholin Cyst/Abscess',
		'Ulcers of the vulva',
		'vaginal cysts',
		'other',
	];
	investigationList = [
		'Complete Blood Picture (CBC)',
		'Blood group',
		'Rhesus factor',
		'Coagulation factors Prothrombin time (PT)',
		'Partial thromboplastin time (PTT)',
		'International normalised ratio (INR) Reproduction assays (Andrology) Semen analysis',
		'Reproduction assays (Gynacology) HCG',
		'Complete Urine analysis',
		'Complete stool analysis',
		'Parasitology assays',
		'General bacteriology',
		'General virology',
		'Liver function tests',
		'Jaundice profile',
		'Renal function tests',
		'Uric acid test',
		'Serum electrolytes',
		'Lipid profile',
		'Cardiac function tests',
		'Pancreatic function tests',
		'Diabetes profile',
		'Cultures & sensitivity',
		'Thyroid function test',
		'Tumor markers',
		'Inflammatory markers',
		'Rheumatology profile',
	];
	dropdownSettings = {
		enableCheckAll: false,
	};
	selectedComplains;
	selectedReasons;
	selectedInvestigation;
	faTrash = faTrash;
	editing: any = {};
	visit: any[];
	marriageYears;
	id: string;
	ranks = [
		'1st',
		'2nd',
		'3rd',
		'4th',
		'5th',
		'6th',
		'7th',
		'8th',
		'9th',
		'10th',
		'11th',
		'12th',
		'13th',
		'14th',
		'15th',
		'16th',
		'17th',
		'18th',
		'19th',
		'20th',
		'21st',
		'22nd',
		'23rd',
		'24th',
		'25th',
		'26th',
		'27th',
		'28th',
		'29th',
		'30th',
	];
	patient;
	imageInModal: string;
	patientForm: FormGroup;
	surgeryForm: FormGroup;
	publishForm: FormGroup;
	ObstetricHistorySpecificForm: FormGroup;
	partAge;
	patientAge;
	edd;
	bmi;
	weeks;
	isOtherReason;
	isOtherComplaint;
	radio: string = 'not Selected';
	contraceptiveHistoryFrom: FormGroup;
	visitsForm: FormGroup;
	checked: Boolean = false;
	modalRef: NgbModalRef;
	constructor(
		private readonly modalService: NgbModal,
		private readonly route: ActivatedRoute,
		private readonly patientService: PatientService,
		private readonly _snackBar: MatSnackBar,
		private readonly fb: FormBuilder
	) { }
	ngOnInit(): void {
		this.initPatientForm();
		this.initSurgeryForm();
		this.initContraceptiveHistoryForm();
		this.initVisitsForm();
		this.initPublishForm();
		this.initObstetricHistorySpecific();
		this.id = this.route.snapshot.params.id;
		if (this.id !== undefined) {
			this.patientService.getPatients(this.id).subscribe(
				(res: res) => {
					this.patient = res.sentObject;

					if (this.patient.birthDate) {
						this.patient.birthDate = formatDate(
							new Date(this.patient.birthDate),
							'yyyy-MM-dd',
							'en'
						);
					}

					if (this.patient.lastVisit) {
						this.patient.lastVisit = formatDate(
							new Date(this.patient.lastVisit),
							'yyyy-MM-dd',
							'en'
						);
					}

					if (this.patient.records.marriageDate) {
						this.patient.records.marriageDate = formatDate(
							new Date(this.patient.records.marriageDate),
							'yyyy-MM-dd',
							'en'
						);
						this.marriageYears = this.calculateAge(
							this.patient.records.marriageDate
						);
					}
					const patientAge = this.calculateAge(this.patient.birthDate);

					this.patientAge = patientAge;

					if (this.patient.records?.partnerBirthDate) {
						this.patient.records.partnerBirthDate = formatDate(
							new Date(this.patient.records.partnerBirthDate),
							'yyyy-MM-dd',
							'en'
						);
						const partnerAge = this.calculateAge(
							this.patient.records?.partnerBirthDate
						);
						this.partAge = partnerAge;
					}
					if (
						this.patient.records.specialHabits &&
						this.patient.records.specialHabits.length > 0
					)
						this.checked = true;
					this.patientForm.patchValue(this.patient);
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);
		}
	}

	dynamicWidth(event) {
		const HTMLNode = event.target;
		const value = event.target.value;
		HTMLNode.style.width = `calc(${value.length}ch + 35px)`;
	}

	isOtherSelected(event, type) {
		if (event === 'other') {
			switch (type) {
				case 'reason':
					this.isOtherReason = true;
					break;
				case 'complaint':
					this.isOtherComplaint = true;
					break;
				default:
					return;
			}
		}
	}
	isOtherUnSelected(event, type) {
		if (event === 'other') {
			switch (type) {
				case 'reason':
					this.isOtherReason = false;
					break;
				case 'complaint':
					this.isOtherComplaint = false;
					break;
				default:
					return;
			}
		}
	}
	addToReasonOfVisits(event) {
		const value = event.target.value;
		const reasonArray = this.visitsForm.get('reasonOfVisit').value;

		this.visitsForm.get('reasonOfVisit').setValue([...reasonArray, value]);
	}

	addToComplaint(event) {
		const value = event.target.value;
		const complaintArray = this.visitsForm.get('complaint').value;

		this.visitsForm.get('complaint').setValue([...complaintArray, value]);
	}

	initPatientForm() {
		this.patientForm = this.fb.group({
			code: [''],
			lastVisit: [''],
			name: [''],
			blood: [''],
			address: [''],
			communication: [''],
			occupation: [''],
			birthDate: [''],
			visitNumber: [''],
			martialStatus: [''],
			records: this.fb.group({
				marriageDate: [''],
				partnerblood: '',
				partnerBirthDate: '',
				partnerOccupation: '',
				drugHistory: '',
				pastHistory: this.fb.group({
					dm: [false],
					hyt: [false],
					heartDisease: [false],
					bleedingDisorders: [false],
					thyroidDisease: [false],
				}),
				familyHistory: this.fb.group({
					consanguinity: [false],
					dm: [false],
					hyt: [false],
					cfmf: [false],
					twins: [false],
				}),
				specialHabits: [''],
				obstetricHistory: this.fb.group({
					g: [''],
					p: this.fb.group({
						first: [''],
						second: [''],
						third: [''],
					}),
					anaesthesiaNote: [''],
					operativeNote: [''],
					complicationOccurred: [''],
					babyWeight: [''],
					hg: [''],
					bp: [''],
					living: [''],
					male: [''],
					female: [''],
				}),
			}),
		});
	}

	initObstetricHistorySpecific() {
		this.ObstetricHistorySpecificForm = this.fb.group({
			rank: [''],
			w: [''],
			number: [''],
			modeOfTermination: [''],
			year: [''],
			anaesthesiaNote: [''],
			operativeNote: [''],
			complicationOccurred: [''],
			babyWeight: [''],
			hg: this.fb.group({
				before: [''],
				after: [''],
			}),
			bp: this.fb.group({
				before: [''],
				after: [''],
			}),
		});
	}

	initPublishForm() {
		this.publishForm = this.fb.group({
			title: [''],
			published: [false],
		});
	}

	cout() { }

	initSurgeryForm() {
		this.surgeryForm = this.fb.group({
			type: ['', Validators.required],
			date: ['', Validators.required],
			notes: ['', Validators.required],
		});
	}

	initContraceptiveHistoryForm() {
		this.contraceptiveHistoryFrom = this.fb.group({
			method: ['', Validators.required],
			duration: this.fb.group({
				from: ['', Validators.required],
				to: ['', Validators.required],
			}),
			effects: ['', Validators.required],
		});
	}

	initVisitsForm() {
		this.visitsForm = this.fb.group({
			date: [''],
			pregnant: [''],
			reasonOfVisit: [''],
			complaint: [''],
			lmp: [''],
			height: [''],
			weight: [''],
			bmi: [this.bmi],
			edd: [this.edd],
			bp: this.fb.group({
				numerator: [''],
				denominator: [''],
			}),
			treatment: [''],
			temp: [''],
			o2Sat: [''],
			investigation: [''],
			notes: [''],
			upload: [''],
			nextVisit: [''],
		});
	}

	publishRecord(modal) {
		const body = this.publishForm.value;
		this.patientService.publishRecord(this.id, body).subscribe(
			(res: res) => {
				modal.close();
				this.publishForm.reset();
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}

	updatePatient() {
		this.patientService
			.updatePatient(this.id, this.patientForm.value)
			.subscribe(
				(res: res) => { },
				(err) => this._snackBar.open(err.error.message, 'close')
			);
	}
	openModal(modal: any, image?: string) {
		if (image) {
			this.imageInModal = image;
			this.modalRef = this.modalService.open(modal, {
				size: 'xl',
				centered: true,
			});
			this.modalRef.result.then(
				(result) => {
					this.visitsForm.reset();
					this.publishForm.reset();
					this.surgeryForm.reset();
				},
				(reason) => {
					this.visitsForm.reset();
					this.publishForm.reset();
					this.surgeryForm.reset();
				}
			);
		} else {
			this.modalRef = this.modalService.open(modal, {
				size: 'xl',
				backdrop: 'static',
				keyboard: false,
				centered: true,
			});
			this.modalRef.result.then(
				(result) => {
					this.visitsForm.reset();
					this.publishForm.reset();
					this.surgeryForm.reset();
					this.edd = null;
					this.bmi = null;
					this.weeks = 0;
				},
				(reason) => {
					this.visitsForm.reset();
					this.publishForm.reset();
					this.surgeryForm.reset();
					this.edd = null;
					this.bmi = null;
					this.weeks = 0;
				}
			);
		}
	}

	calculateAge(date) {
		const today = new Date();
		const birthDate = new Date(date);
		let age = today.getFullYear() - birthDate.getFullYear();
		const m = today.getMonth() - birthDate.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--;
		}
		return age;
	}

	calcEdd(event) {
		const value = new Date(event.target.value);
		this.edd = formatDate(
			add(value, { months: 9, days: 7 }),
			'yyyy-MM-dd',
			'en'
		);
		this.weeks = differenceInCalendarWeeks(new Date(), value);
	}

	calcBMI(height, weight) {
		this.bmi = Math.ceil(weight / (height / 100) ** 2);
	}

	showAge(event: Event, type: string) {
		const value = (<HTMLDataElement>event.target).value;
		const age = this.calculateAge(value);
		switch (type) {
			case 'patient':
				this.patientAge = age;
				break;
			case 'partner':
				this.partAge = age;
				break;
			case 'marriage':
				this.marriageYears = age;
				break;
		}
	}

	onImagePic(event: Event, type: string) {
		const file = (event.target as HTMLInputElement).files[0];
		const reader = new FileReader();
		reader.onload = () => {
			this.visitsForm.get(type).patchValue(reader.result as string);
		};
		reader.readAsDataURL(file);
	}

	addVisit(modal) {
		const body = {
			...this.visitsForm.value,
			edd: this.edd,
			bmi: this.bmi,
		};
		this.patientService.addVisit(this.id, body).subscribe(
			(res: res) => {
				this.patient.records.visits = [
					...this.patient.records.visits,

					{
						_id: res.sentObject,
						...this.visitsForm.value,
					},
				];
				this.edd = null;
				this.bmi = null;
				modal.close();
				this.visitsForm.reset({});
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}

	addContraceptiveHistory(modal) {
		this.patientService
			.addContraceptiveHistory(this.id, this.contraceptiveHistoryFrom.value)
			.subscribe(
				(res: res) => {
					this.patient.records.contraceptiveHistory = [
						...this.patient.records.contraceptiveHistory,
						{
							...res.sentObject,
						},
					];

					modal.close();
					this.contraceptiveHistoryFrom.reset({ method: 'Choose Method' });
				},
				(err) => this._snackBar.open(err.error.message)
			);
	}
	AddObstetricHistory() {
		this.patientService
			.addObstetricHistory(this.id, this.ObstetricHistorySpecificForm.value)
			.subscribe((res: res) => {
				this.patient.records.obstetricHistory.specific = [
					...res.sentObject,
				];
				this.modalRef.close();
			});
	}

	updateObstetricHistory(event, c, index) {
		const value = event.target.value;
		const cell = c.split('.');
		const id = this.patient.records.obstetricHistory.specific[index]._id;
		this.patientService
			.updateObstetricHistory(this.id, id, {
				[c]: value,
			})
			.subscribe((res: res) => {
				if (cell.length > 1) {
					this.patient.records.obstetricHistory.specific[index][cell[0]][
						cell[1]
					] = value;
				} else {
					this.patient.records.obstetricHistory.specific[index][cell[0]] =
						value;
				}
			});
	}

	deleteObstetricHistory(id) {
		this.patientService
			.DeleteObstetricHistory(this.id, id)
			.subscribe((res: res) => {
				this.patient.records.obstetricHistory.specific = [
					...res.sentObject,
				];
			});
	}

	addSurgery(modal) {
		this.patientService.addSurgery(this.id, this.surgeryForm.value).subscribe(
			(res: res) => {
				this.patient.records.surgeries = [
					...this.patient.records.surgeries,
					res.sentObject,
				];

				modal.close();
			},
			(err) => this._snackBar.open(err.error.message)
		);
	}

	updateDropDownValues(cell, rowIndex) {
		const id = this.patient.records.visits[rowIndex]._id;
		let value;
		switch (cell) {
			case 'investigation':
				value = this.selectedInvestigation;
				break;

			case 'complaint':
				value = this.selectedComplains;
				break;
			case 'reasonOfVisit':
				value = this.selectedReasons;
				break;
			default:
				return;
		}

		this.patientService
			.updateVisits(this.id, id, {
				[cell]: value,
			})
			.subscribe(
				(res: res) => {
					this.patient.records.visits[rowIndex][cell] = [...value];
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);

		this.editing[rowIndex + '-' + cell] = false;
	}

	updateValue(
		event: Event,
		cell: string,
		rowIndex: number,
		type: string = '',
		isFile?: Boolean
	) {
		const value = (<any>event.target).value;
		const id = this.patient.records.visits[rowIndex]._id;
		if (type || type !== '') {

		} else if (isFile) {
			const file = (event.target as HTMLInputElement).files[0];
			const reader = new FileReader();
			let imgBase64;
			reader.onload = () => {
				imgBase64 = reader.result as string;
				this.patientService
					.updateVisits(this.id, id, {
						[cell]: imgBase64,
					})
					.subscribe(
						(res: res) => {
							this.patient.records.visits[rowIndex][cell] = imgBase64;
						},
						(err) => this._snackBar.open(err.error.message, 'close')
					);
			};
			reader.readAsDataURL(file);
		} else {
			const oldValue = this.patient.records.visits[rowIndex][cell];
			if (oldValue === value)
				return (this.editing[rowIndex + '-' + cell] = false);

			this.patientService
				.updateVisits(this.id, id, {
					[cell]: value,
				})
				.subscribe(
					(res: res) => {
						this.patient.records.visits[rowIndex][cell] = value;
					},
					(err) => this._snackBar.open(err.error.message, 'close')
				);
		}
		this.editing[rowIndex + '-' + cell] = false;
	}

	updateBp(cell: string, rowIndex: number, numerator, denominator) {
		const id = this.patient.records.visits[rowIndex]._id;

		const object = {
			numerator,
			denominator,
		};

		const numeratorOldValue =
			this.patient.records.visits[rowIndex][cell].numerator;
		const denominatorOldValue =
			this.patient.records.visits[rowIndex][cell].denominator;
		if (
			numeratorOldValue === numerator &&
			denominatorOldValue === denominator
		)
			return (this.editing[rowIndex + '-' + cell] = false);

		this.patientService
			.updateVisits(this.id, id, {
				[cell]: object,
			})
			.subscribe(
				(res: res) => {
					this.patient.records.visits[rowIndex][cell] =
						Object.assign(object);
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);
		this.editing[rowIndex + '-' + cell] = false;

	}

	updateContraceptiveHistory(
		event: Event,
		cell: string,
		rowIndex: number,
		isDate?: boolean,
		fromValue?: Date | string,
		toValue?: Date | string
	) {
		const id = this.patient.records.contraceptiveHistory[rowIndex]._id;
		if (isDate) {
			const fromOldValue =
				this.patient.records.contraceptiveHistory[rowIndex][cell].from;
			const toOldValue =
				this.patient.records.contraceptiveHistory[rowIndex][cell].to;

			if (fromOldValue === fromValue && toOldValue === toValue)
				return (this.editing[rowIndex + '-' + cell] = false);

			this.patientService
				.updateContraceptiveHistory(this.id, id, {
					duration: { from: fromValue, to: toValue },
				})
				.subscribe(
					(res: res) => {
						this.patient.records.contraceptiveHistory[rowIndex][cell] =
							Object.create({ from: fromValue, to: toValue });
					},
					(err) => this._snackBar.open(err.error.message, 'close')
				);
		} else {
			const value = (<HTMLInputElement>event.target).value;
			const oldValue =
				this.patient.records.contraceptiveHistory[rowIndex][cell];
			if (oldValue === value)
				return (this.editing[rowIndex + '-' + cell] = false);
			this.patientService
				.updateContraceptiveHistory(this.id, id, {
					[cell]: value,
				})
				.subscribe(
					(res: res) => {
						this.patient.records.contraceptiveHistory[rowIndex][cell] =
							value;
					},
					(err) => this._snackBar.open(err.error.message, 'close')
				);
		}

		this.editing[rowIndex + '-' + cell] = false;
	}

	updateSurgery(event: Event, cell: string, rowIndex: number) {
		const value = (<HTMLInputElement>event.target).value;
		const id = this.patient.records.surgeries[rowIndex]._id;

		this.patientService
			.updateSurgery(this.id, id, {
				[cell]: value,
			})
			.subscribe(
				(res: res) => {
					this.patient.records.surgeries[rowIndex][cell] = value;
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);
		this.editing[rowIndex + '-' + cell] = false;
	}

	deleteVisit(id, index) {
		this.patientService.deleteVisit(this.id, id).subscribe(
			(res: res) => {
				this.patient.records.visits = [
					...this.patient.records.visits.filter((c) => c._id !== id),
				];
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}

	deleteContraceptiveHistory(id, index) {
		this.patientService.deleteContraceptiveHistory(this.id, id).subscribe(
			(res: res) => {
				this.patient.records.contraceptiveHistory = [
					...this.patient.records.contraceptiveHistory.filter(
						(c) => c._id !== id
					),
				];
			},
			(err) => this._snackBar.open(err.error.message)
		);
	}

	deleteSurgery(id, rowIndex) {
		this.patientService.deleteSurgery(this.id, id).subscribe(
			(res: res) => {
				this.patient.records.surgeries = [
					...this.patient.records.surgeries.filter((c) => c._id !== id),
				];
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}
}
