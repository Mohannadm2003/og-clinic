import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { DoctorService } from 'src/app/Shared/services/doctor/doctor.service';

@Component({
	selector: 'app-clinic-login',
	templateUrl: './clinic-login.component.html',
	styleUrls: ['./clinic-login.component.css'],
})
export class ClinicLoginComponent implements OnInit {
	logInForm: FormGroup;
	constructor(
		private doctorService: DoctorService,
		private router: Router,
		private _snackBar: MatSnackBar
	) {}

	ngOnInit(): void {
    this.logInForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
		const isAuth = this.doctorService.isMyClinic();
		if (isAuth) {
      this.router.navigate(['/', 'myclinic', 'patient']);
			return
		}
	}

	login() {
		this.doctorService.myClinicLogIn(this.logInForm.value).subscribe(
			(res: { sentObject; token; message; role; enabled; inMyClinic }) => {
				this.doctorService.saveAuthData(
					res.token,
					Object.assign(res.sentObject),
					res.role,
					res.enabled,
					res.inMyClinic
				);
				this.doctorService.isAuth = true;
				this.doctorService.isAuthlistener.next(true);
				this.router.navigate(['/', 'myclinic', 'patient']);
			},
			(err) => {
				this._snackBar.open(err.error.message, 'close');
			}
		);
	}
}
