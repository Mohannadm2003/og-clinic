import { Component, OnInit } from '@angular/core';
import {
	AbstractControl,
	FormBuilder,
	FormGroup,
	ValidationErrors,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { res } from 'src/app/Shared/models/res.model';
import { DoctorService } from 'src/app/Shared/services/doctor/doctor.service';

@Component({
	selector: 'app-edit-doctor',
	templateUrl: './edit-doctor.component.html',
	styleUrls: ['./edit-doctor.component.css'],
})
export class EditDoctorComponent implements OnInit {
	id = this.route.snapshot.paramMap.get('id');
	doctor;
	doctorForm: FormGroup;
	passwordForm: FormGroup;
	oldEmail: string;
	imgLink: any = 'https://via.placeholder.com/85';
	countries: any[];
	cites: any[];
	constructor(
		private doctorService: DoctorService,
		private route: ActivatedRoute,
		private fb: FormBuilder,
		private _snackBar: MatSnackBar,
		private router: Router
	) {}

	ngOnInit(): void {
		this.initDoctorForm();
		this.initUpdatePasswordForm();
		this.doctorService.getCountries().subscribe((res: res) => {
			this.countries = res.sentObject;
		});
		if (!this.id) {
			this.router.navigate(['/myclinic', 'patient']);
		}

		this.doctorService.getDoctor(this.id).subscribe(
			(res: res) => {
				this.doctor = res.sentObject;
				this.imgLink = this.doctor.img;
				this.oldEmail = this.doctor.email;
				if (this.doctor.country) {
					const code = this.doctor.country.split(',')[1];
					this.doctorService
						.getCitiesOFCountry(code)
						.subscribe((res: res) => {
							this.cites = res.sentObject;
						});
				}
				this.doctorForm.patchValue(this.doctor);
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}

	initDoctorForm() {
		this.doctorForm = this.fb.group({
			firstName: [''],
			lastName: [''],
			gender: [''],
			phone: [''],
			email: [''],
			img: [''],
			speciality: [''],
			clinicName: [''],
			clinicAddress: [''],
			facebook: [''],
			whatsapp: [''],
			qualifications: [''],
			services: [''],
			city: [''],
			country: [''],
			subspeciality: [''],
		});
	}

	showCity(event) {
		const value = event.target.value;
		const isoCode = value.split(',')[1];
		this.doctorService.getCitiesOFCountry(isoCode).subscribe((res: res) => {
			this.cites = res.sentObject;
		});
	}

	onImageHandle(event) {
		const file = (event.target as HTMLInputElement).files[0];
		const reader = new FileReader();
		this.doctorForm.patchValue({ img: file });
		this.doctorForm.controls.img.updateValueAndValidity();
		reader.onload = () => {
			this.imgLink = reader.result;
		};
		reader.readAsDataURL(file);
	}

	initUpdatePasswordForm() {
		this.passwordForm = this.fb.group(
			{
				oldPassword: ['',[Validators.required]],
				oldPasswordConfirm: ['',[Validators.required]],
				newPassword: ['',[Validators.required]],
			},
			{validators:this.passwordMatchingValidatior}
		);
	}

	passwordMatchingValidatior(
		control: AbstractControl
	): ValidationErrors | null {
		const password = control.get('oldPassword');
		const confirmPassword = control.get('oldPasswordConfirm');
		return password?.value === confirmPassword?.value
			? null
			: { notmatched: true };
	}

	updateDoctor() {
		const formData = new FormData();
		for (let key in this.doctorForm.value) {
			if (key === 'email' && this.oldEmail === this.doctorForm.value[key]) {
			} else {
				formData.append(key, this.doctorForm.value[key]);
			}
		}
		this.doctorService.updateDoctor(this.id, formData).subscribe(
			(res: res) => {},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}
	updatePass() {
		this.doctorService
			.updatePassword(this.id, this.passwordForm.value)
			.subscribe(
				(res: res) => {},
				(err) => this._snackBar.open(err.error.message, 'close')
			);
	}
}
