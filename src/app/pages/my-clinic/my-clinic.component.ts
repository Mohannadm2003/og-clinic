import { Component, OnInit } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PatientService } from 'src/app/Shared/services/patients/patient.service';
import { res } from 'src/app/Shared/models/res.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { faEdit, faEye } from '@fortawesome/free-regular-svg-icons';
import { faExternalLinkAlt, faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
	selector: 'app-my-clinic',
	templateUrl: './my-clinic.component.html',
	styleUrls: ['./my-clinic.component.css'],
})
export class MyClinicComponent implements OnInit {
	pager: { page: number; pages: number; pageSize: number; total: number };
	patientsList: any[];
	faEye = faEye;

	faExternalLinkAlt = faExternalLinkAlt;

	faTrash = faTrash;
	ColumnMode = ColumnMode;
	modalRef;
	closeResult = '';
	patientForm: FormGroup;
	now: Date = new Date(Date.now());
	constructor(
		private readonly modalService: NgbModal,
		private readonly fb: FormBuilder,
		private readonly patientService: PatientService,
		private readonly _snackBar: MatSnackBar
	) {}

	ngOnInit(): void {
		this.pager = {
			page: 1,
			pages: 0,
			pageSize: 12,
			total: 0,
		};
		this.getPatients('');
		this.patientForm = this.fb.group({
			code: ['', [Validators.required]],
			name: ['', [Validators.required]],
			address: ['', [Validators.required]],
			lastVisit: [''],
			visitNumber: [''],
		});
	}

	setPage(pageInfo: any) {
		this.pager.page = pageInfo.offset + 1;
		this.getPatients('');
	}

	openModal(content: any) {
		this.modalService.open(content, {
			size: 'xl',
			centered: true,
		});
	}
	createPatient(modal) {
		this.patientService.createPatient(this.patientForm.value).subscribe(
			(res: res) => {
				this.getPatients('');
				this.patientForm.reset();
				modal.close();
			},
			(err) => {
				this._snackBar.open(err.error.message, 'close');
			}
		);
	}

	getPatients(code: string) {
		this.patientService
			.getPatients('', this.pager.pageSize, this.pager.page, code)
			.subscribe(
				(res: res) => {
					if (
						res.sentObject !== null &&
						res.sentObject.length === undefined
					) {
						this.patientsList = [res.sentObject];
					} else if (res.sentObject === null) {
						this.patientsList = [];
					} else {
						this.patientsList = [...res.sentObject];
					}
					this.pager.total = res.count;
					this.pager.pages = res.pages;
					this.patientsList.forEach((p) => {
						p.age = Math.abs(
							new Date(
								new Date('2003').valueOf() -
									new Date(p.birthDate).valueOf()
							).getUTCFullYear() - 1970
						);
					});
				},
				(err) => {
					this._snackBar.open(err.error.message, 'close');
				}
			);
	}
	deletePatient(id, index) {
		this.patientService.deletePatient(id).subscribe(
			(res: res) => {
				this.patientsList.splice(index, 1);
				this.patientsList = [...this.patientsList];
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}

	search(code: string, name: string) {
		this.patientService.search(code, name).subscribe(
			(res: res) => {
				if (
					res.sentObject !== null &&
					res.sentObject.length === undefined
				) {
					this.patientsList = [res.sentObject];
				} else if (res.sentObject === null) {
					this.patientsList = [];
				} else {
					this.patientsList = [...res.sentObject];
				}
				this.pager.total = res.count;
				this.pager.pages = res.pages;
				this.patientsList.forEach((p) => {
					p.age = Math.abs(
						new Date(
							new Date('2003').valueOf() -
								new Date(p.birthDate).valueOf()
						).getUTCFullYear() - 1970
					);
				});
			},
			(err) => this._snackBar.open(err.error.message)
		);
	}
}
