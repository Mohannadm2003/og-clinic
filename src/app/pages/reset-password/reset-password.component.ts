import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { DoctorService } from 'src/app/Shared/services/doctor/doctor.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  resetPassword: FormGroup;
  isDone;
  token = this.route.snapshot.queryParamMap.get('token');

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private doctorService: DoctorService,
    private _sb: MatSnackBar
  ) { }

  ngOnInit(): void {
    console.log(this.route.snapshot.queryParamMap,this.route.snapshot.paramMap)
    this.initForm()
  }

  initForm() {
    this.resetPassword = this.fb.group({
      newPassword: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
    },
      { validators: this.passwordMatchingValidatior });
  }

  passwordMatchingValidatior(
    control: AbstractControl
  ): ValidationErrors | null {
    const password = control.get('newPassword');
    const confirmPassword = control.get('confirmPassword');
    console.log('working')
    return password?.value === confirmPassword?.value
      ? null
      : { notmatched: true };
  }


  reset() {
    this.doctorService.resetPassword({ ...this.resetPassword.value, token: `Bearer ${this.token}` }).subscribe((res) => { this.isDone = true }, err => { this._sb.open(err.error.message, 'close') })
  }
}
