import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ColumnMode, id } from '@swimlane/ngx-datatable';
import { res } from 'src/app/Shared/models/res.model';
import { DoctorService } from 'src/app/Shared/services/doctor/doctor.service';
@Component({
	selector: 'app-all-doctors',
	templateUrl: './all-doctors.component.html',
	styleUrls: ['./all-doctors.component.css','../../../assets/styles/ngx-datatable.scss'],
})
export class AllDoctorsComponent implements OnInit {
	doctorList = [];
	pager = {
		total: 0,
		pageSize: 20,
		pages: 0,
	};
	editing = {};
	ColumnMode = ColumnMode;
	constructor(
		private doctorService: DoctorService,
		private _snackBar: MatSnackBar
	) {}

	ngOnInit(): void {
		this.doctorService.getAllDoctors().subscribe(
			(res: res) => {
				this.doctorList = res.sentObject;
				this.pager.total = res.count;
				this.pager.pages = res.pages;
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}

	setPage(event) {
    this.doctorService.getAllDoctors(20, event.offset + 1).subscribe(
			(res: res) => {
				this.pager.pageSize = 20;
				this.pager.total = res.count;
				this.doctorList = res.sentObject;
			},
			(err) => {
				this._snackBar.open(err.error.message, 'close');
			}
		);
  }

	updateValue(event, cell, rowIndex) {
		const value = event.target.value;
		const id = this.doctorList[rowIndex]._id;
		if (cell === 'enabled') {
			this.doctorService.updateDoctorTools(id, { [cell]: value }).subscribe(
				(res: res) => {
					this.doctorList[rowIndex][cell] = value;
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);
		} else if (cell === 'subsExpDate') {
			this.doctorService.updateSubsExpDate(id, { [cell]: value }).subscribe(
				(res: res) => {
					this.doctorList[rowIndex][cell] = value;
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);
		} else {
			this.doctorService.updateDoctorClinic(id, { [cell]: value }).subscribe(
				(res) => {
					this.doctorList[rowIndex][cell] = value;
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);
		}
		this.editing[rowIndex + '-' + cell] = false;
	}
}
