import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorToolsHomeComponent } from './doctor-tools-home.component';

describe('DoctorToolsHomeComponent', () => {
  let component: DoctorToolsHomeComponent;
  let fixture: ComponentFixture<DoctorToolsHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorToolsHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorToolsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
