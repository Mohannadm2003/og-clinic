import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	OnInit,
} from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { PostService } from 'src/app/Shared/services/post/post.service';
import { faEdit, faTrashAlt } from '@fortawesome/free-regular-svg-icons';
@Component({
	selector: 'app-blogs',
	templateUrl: './blogs.component.html',
	styleUrls: ['./blogs.component.css'],
})
export class BlogsComponent implements OnInit, AfterViewInit {
	ColumnMode = ColumnMode;
	editing: any = {};
	blogs: any[];
	faEdit = faEdit;
	faTrashAlt = faTrashAlt;
	constructor(
		private modalService: NgbModal,
		private postService: PostService,
		private _snackBar: MatSnackBar,
		private changeDetector: ChangeDetectorRef
	) {}

	pager: any= {
    page: 1,
    pageSize: 12,
    total: 0,
  };

	ngOnInit(): void {

	}

	ngAfterViewInit(): void {
		 this.pager = {
		 	page: 1,
		 	pages: 0,
		 	pageSize: 12,
		 	total: 0,
      pageNumber:0
		 };
		 this.postService.getAdminPost(12, this.pager.page).subscribe(
		 	(res: any) => {
		 		this.blogs = res.sentObject;
		 		this.pager.total = res.count;
		 		 this.changeDetector.detectChanges();
		 	},
		 	(err) => {
		 		this._snackBar.open(err.error.message, 'close');
		 	}
		 );
	}

	updateValue(event: any, cell: string, rowIndex: number) {
		this.editing[rowIndex + '-' + cell] = false;
		const form = new FormData();
		const oldValue = this.blogs[rowIndex].published;
		this.blogs[rowIndex].published = event.target.value;

		this.postService
			.publishUnPublishPost(this.blogs[rowIndex]._id, {
				published: event.target.value,
			})
			.subscribe(
				(res: any) => {
					this.blogs[rowIndex].published = event.target.value;
				},
				(err) => {
					this.blogs[rowIndex].published = oldValue;

					this._snackBar.open(err.error.message, 'close');
				}
			);
	}

	setPage(pageInfo: any) {
		this.pager.page = pageInfo.offset + 1;
		this.postService
			.getAdminPost(this.pager.pageSize, this.pager.page)
			.subscribe(
				(res: any) => {
					this.blogs = res.sentObject;
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);
	}
	deletePost(id: string, index: number) {
		this.postService.deletePost(id).subscribe(
			(res) => {
				this.blogs = [...this.blogs.slice(index)];
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}
}
