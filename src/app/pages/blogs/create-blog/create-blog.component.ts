// import 'froala-editor/js/plugins/align.min.js';
// import 'froala-editor/js/plugins/colors.min.js';
// import 'froala-editor/js/plugins/font_size.min.js';
// import 'froala-editor/js/plugins/image.min.js';
// import 'froala-editor/js/plugins/image_manager.min.js';
// import 'froala-editor/js/plugins/line_breaker.min.js';
// import 'froala-editor/js/plugins/link.min.js';
// import 'froala-editor/js/plugins/lists.min.js';
// import 'froala-editor/js/plugins/paragraph_format.min.js';
// import 'froala-editor/js/plugins/paragraph_style.min.js';
// import 'froala-editor/js/plugins/table.min.js';
// import 'froala-editor/js/plugins/url.min.js';
// import 'froala-editor/js/plugins/word_paste.min.js';
// import 'froala-editor/js/plugins/file.min.js';
// import 'froala-editor/js/plugins/quick_insert.min.js';
// import 'froala-editor/js/plugins/save.min.js';
// import 'froala-editor/js/third_party/image_tui.min.js';
// import 'froala-editor/js/third_party/embedly.min.js';
// import 'froala-editor/js/third_party/showdown.min.js';

import 'froala-editor/js/plugins.pkgd.min.js';
// import 'froala-editor/js/languages/ar.js';

import { Component, OnInit, } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { PostService } from 'src/app/Shared/services/post/post.service';
import { CookieService } from 'ngx-cookie-service';

import { postType } from '../../../Shared/enum/types.enum'

// import { validator } from './mime-validation';
// import 'froala-editor/js/plugins/video.min.js';
// import 'froala-editor/js/plugins/paragraph_style.min.js';
@Component({
	selector: 'app-create-blog',
	templateUrl: './create-blog.component.html',
	styleUrls: ['./create-blog.component.scss'],
})
export class CreateBlogComponent implements OnInit {
	ColumnMode = ColumnMode;
	editing: any = {};
	isDrug: boolean = false;
	id: string;
	postFormGroup: FormGroup;
	isIndex = false;
	// @ViewChild('froalaEditor', { static: false }) froalaEditor: ElementRef;
	options = {
		requestHeaders: {
			Authorization: 'Bearer ' + this.cookieService.get('token')
		},

		charCounterCount: true,
		// Set the image upload parameter.
		// imageUploadParam: 'image_param',

		// Set the image upload URL.
		imageUploadURL: '/api/posts/post/uploadImg',

		// Additional upload params.
		// imageUploadParams: { id: typeof id !== 'undefined' ? id : 'null' },

		// Set request type.
		imageUploadMethod: 'POST',

		// Set max image size to 5MB.
		imageMaxSize: 1024 * 1024,

		// Allow to upload PNG and JPG.
		imageAllowedTypes: ['jpeg', 'jpg', 'png'],

		// Set the video upload URL.
		// videoUploadURL: '/blog/posts/upload_image',

		// Additional upload params.
		// videoUploadParams: { id: typeof id !== 'undefined' ? id : 'null' },

		// Set request type.
		// videoUploadMethod: 'POST',

		// Set max video size to 50MB.
		// videoMaxSize: 50 * 1024 * 1024,

		// Allow to upload MP4, WEBM and OGG
		// videoAllowedTypes: ['webm', 'jpg', 'ogg', 'mp4'],

		// Set the file upload parameter.
		// fileUploadParam: 'file',

		// Set the file upload URL.
		fileUploadURL:'/api/posts/post/uploadImg',

		// Additional upload params.
		// fileUploadParams: { id: typeof id !== 'undefined' ? id : 'null' },

		// Set request type.
		fileUploadMethod: 'POST',

		// Set max file size to 20MB.
		fileMaxSize: 20 * 1024 * 1024,

		// Allow to upload any file.
		fileAllowedTypes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document'],

		events: {
			'initialized'() {
				console.log('initialized');
			},
			'image.beforeUpload'(images) {
				// if (images.length) {
				// Create a File Reader.
				const reader = new FileReader();
				// Set the reader to insert images when they are loaded.
				reader.onload = (ev) => {
					const result = ev.target.result;
					// this.image.insert(result, null, null, this.image.get());
					console.log(ev.target.result);
				};
				// Read image as base64.
				reader.readAsDataURL(images[0]);
				// }
				// Stop default upload chain. change to true, to call the backend
				return true;
			},
			'file.beforeUpload'(files) {
				if (files.length) {
					// const fileName = files[0].name;
          // console.log(files);
					// this.the_name = fileName;
					// console.log(fileName);
				}
				return true;
			},
			'image.removed': (imgs) => {
				const url = imgs[0].getAttribute('src').split('/')[2];
				this.postService.deleteImages(url).subscribe(() => { });
				// const fileName = url.substring(url.lastIndexOf('/') + 1, url.length);
			},
			'image.replaced': function (img) {
				// Do something here.
				// this is the editor instance.
				console.log(this);
			},
      'file.unlink':  (link)=>{
       const name = link.getAttribute('href').split('/')[2];
        this.postService.deleteImages(name).subscribe(()=>{})
        return true;
      }
		}
	};


	poweredByFroala = `<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>`

	constructor(
		private readonly postService: PostService,
		private readonly fb: FormBuilder,
		private readonly route: ActivatedRoute,
		private readonly router: Router,
		private readonly _sb: MatSnackBar,
		private readonly cookieService: CookieService
	) { }
	get drugForms() {
		return this.postFormGroup.controls.drugForms as FormArray;
	}
	ngOnInit(): void {
		this.id = this.route.snapshot.paramMap.get('id');
		this.initFroalaOptions('');
		this.postFormGroup = this.fb.group({
			title: ['', Validators.maxLength(50)],
			body: [''],
			category: ['Patient'],
			subCategory: [''],
			img: [''],
			drugType: [''],
			drugForms: this.fb.array([]),
		});
		if (this.id) {
			this.postService.getPost(this.id).subscribe((res: any) => {
				this.postFormGroup.patchValue(res.sentObject);
				this.postFormGroup.controls.body.setValue(res.sentObject.body);
				if (res.sentObject.category === 'Drug') {
					this.isDrug = true;
					for (let i of res.sentObject.drugForms) {
						this.drugForms.push(
							this.fb.group({
								index: [i.index],
								form: [i.form],
								product: [i.product],
							})
						);
					}
					if (res.sentObject.subCategory === 'Drug index') {
						this.isIndex = true;
					}
				}
			});
		}
	}

	checkDrug(event: any) {
		const val = event.target.value
		if (val === postType.drug) {
			this.isDrug = true;
			this.postFormGroup.controls.category.setValue(val);
			return;
		} else {
			this.postFormGroup.get('subCategory').setValue('');
			this.postFormGroup.get('drugType').setValue('');
			this.postFormGroup.get('drugForms').setValue([]);
		}
		this.postFormGroup.controls.category.setValue(val);
		this.isDrug = false;
	}

	checkIndex(event) {
		const value = event.target.value;
		if (value === 'Drug index') this.isIndex = true;
		else this.isIndex = false;
	}

	onImagePic(event: Event) {
		const file = (event.target as HTMLInputElement).files[0];
		const reader = new FileReader();
		this.postFormGroup.patchValue({ img: file });
		this.postFormGroup.controls.img.updateValueAndValidity();
		reader.onload = () => { };
		reader.readAsBinaryString(file);
	}
	savePost() {
		this.postFormGroup.value.body.replace(this.poweredByFroala, '');
		const body = {
			...this.postFormGroup.value,
			drugForms: this.drugForms.value,
		};
		const form = new FormData();
		for (let key in body) {
			if (key === 'drugForms') {
				form.append(key, JSON.stringify(body[key]));
			} else {
				form.append(key, body[key]);
			}
		}
		if (this.id) {
			this.postService.updatePost(this.id, form).subscribe(
				(res: any) => {
					this.router.navigate(['myclinic', 'posts']);
				}, err => this._sb.open(err.error.message, 'close')
			);
		} else {
			this.postService.createPost(form).subscribe(
				(res: any) => {
					this.router.navigate(['myclinic', 'posts']);
				}, err => this._sb.open(err.error.message, 'close')
			);
		}
	}
	addForm() {
		this.drugForms.push(
			this.fb.group({
				index: [
					this.drugForms.controls.length == 0
						? 0
						: this.drugForms.controls.length,
				],
				form: [''],
				product: [''],
			})
		);
	}
	deleteForm(i: number) {
		this.drugForms.removeAt(i);
	}

	initFroalaOptions(id: string) {
		const instance = this;
		return
	}
}
