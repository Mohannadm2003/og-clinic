import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalReferralNetworkComponent } from './medical-referral-network.component';

describe('MedicalReferralNetworkComponent', () => {
  let component: MedicalReferralNetworkComponent;
  let fixture: ComponentFixture<MedicalReferralNetworkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalReferralNetworkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalReferralNetworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
