import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { res } from 'src/app/Shared/models/res.model';
import { DoctorService } from 'src/app/Shared/services/doctor/doctor.service';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
@Component({
	selector: 'app-medical-referral-network',
	templateUrl: './medical-referral-network.component.html',
	styleUrls: ['./medical-referral-network.component.css'],
})
export class MedicalReferralNetworkComponent implements OnInit {
	medicalNetworkForm: FormGroup;
	faTrash = faTrash;
	ColumnMode = ColumnMode;
	editing = {};
	medicalNetwork;
	modalRef: NgbModalRef;
	constructor(
		private fb: FormBuilder,
		private modalService: NgbModal,
		private doctorService: DoctorService,
		private _snackBar: MatSnackBar
	) {}

	ngOnInit(): void {
		this.initMedicalNetwork();
		this.doctorService.getMedicalNetwork().subscribe((res: res) => {
			this.medicalNetwork = res.sentObject;
		});
	}

	initMedicalNetwork() {
		this.medicalNetworkForm = this.fb.group({
			name: [''],
			speciality: [''],
			communications: [''],
		});
	}

	addToMedicalNetwork() {
		this.doctorService
			.addMedicalNetwork(this.medicalNetworkForm.value)
			.subscribe(
				(res: res) => {
					this.medicalNetwork = [...res.sentObject];
					this.modalRef.close();
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);
	}

	updateValue(event, cell, rowIndex) {
		const value = event.target.value;
		const id = this.medicalNetwork[rowIndex]._id;
		this.doctorService.updateMedicalNetwork(id, { [cell]: value }).subscribe(
			(res) => {
				this.medicalNetwork[rowIndex][cell] = value;
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
		this.editing[rowIndex + '-' + cell] = false;
	}

	openModal(modal) {
		this.modalRef = this.modalService.open(modal, {
			size: 'xl',
			centered: true,
		});
		this.modalRef.result.then(
			(result) => {
				this.medicalNetworkForm.reset();
			},
			(reason) => {
				this.medicalNetworkForm.reset();
			}
		);
	}

	deleteMedicalNetwork(id, index) {
		this.doctorService.deleteMedicalNetwork(id).subscribe(
			(res) => {
				this.medicalNetwork = this.medicalNetwork.filter((m, i) => {
					return i !== index;
				});
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}
}
