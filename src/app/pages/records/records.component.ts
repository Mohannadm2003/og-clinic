import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DomSanitizer } from '@angular/platform-browser';
import { res } from 'src/app/Shared/models/res.model';
import { PostService } from 'src/app/Shared/services/post/post.service';

@Component({
	selector: 'app-records',
	templateUrl: './records.component.html',
	styleUrls: ['./records.component.css'],
})
export class RecordsComponent implements OnInit {
	posts: any[];
	pager = {
		pageSize: 0,
		length: 0,
	};
	constructor(
		private postService: PostService,
		// private sanitizer: DomSanitizer,
		private _snackBar: MatSnackBar
	) {}

	search(val) {
		this.postService.searchInRecords(val).subscribe((res: res) => {
			this.posts = res.sentObject;
		});
	}

	ngOnInit(): void {
		this.postService.getRecords(12, 1).subscribe(
			(res: res) => {
				this.pager.pageSize = 12;
				this.pager.length = res.count;
				this.posts = res.sentObject;
				// this.posts.forEach((p) => {
				// 	p.img = this.sanitizer.bypassSecurityTrustUrl(p.img);
				// });
			},
			(err) => {
				this._snackBar.open(err.error.message, 'close');
			}
		);
	}

	handlePage(event: PageEvent) {
		this.postService.getRecords(12, event.pageIndex + 1).subscribe(
			(res: res) => {
				this.pager.pageSize = 12;
				this.pager.length = res.count;
				this.posts = res.sentObject;
				this.posts.forEach((p) => {
					// p.img = this.sanitizer.bypassSecurityTrustUrl(
					// 	'http://localhost:3000' + p.img
					// );
				});
			},
			(err) => {
				this._snackBar.open(err.error.message, 'close');
			}
		);
	}
}
