import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrugCatsComponent } from './drug-cats.component';

describe('DrugCatsComponent', () => {
  let component: DrugCatsComponent;
  let fixture: ComponentFixture<DrugCatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrugCatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrugCatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
