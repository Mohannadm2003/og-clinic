import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { postType } from 'src/app/Shared/enum/types.enum';
import { PostService } from 'src/app/Shared/services/post/post.service';
import { res } from '../../Shared/models/res.model';

@Component({
	selector: 'app-drug-cats',
	templateUrl: './drug-cats.component.html',
	styleUrls: ['./drug-cats.component.css'],
})
export class DrugCatsComponent implements OnInit {
	obstetric;
  gynacology;
  drugs;

  constructor(
		private postService: PostService,
		private _snackBar: MatSnackBar
	) {}

	ngOnInit(): void {
		this.postService
			.getPost('', 12, 1, postType.drug, 'Drug index')
			.subscribe(
				(res: res) => {
          this.gynacology = res.sentObject.gynacology;
          this.obstetric = res.sentObject.obstetric;
          this.drugs = res.sentObject.drugs;

				},
				(err) => {
					this._snackBar.open(err.error.message, 'close');
				}
			);
	}
}
