import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { res } from 'src/app/Shared/models/res.model';
import { PatientService } from 'src/app/Shared/services/patients/patient.service';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
	selector: 'app-icsi',
	templateUrl: './icsi.component.html',
	styleUrls: ['./icsi.component.css'],
})
export class IcsiComponent implements OnInit {
	ColumnMode = ColumnMode;
	faTrash = faTrash;
	patient;
	guideMap = new Map();
	ranks = [
		'1st',
		'2nd',
		'3rd',
		'4th',
		'5th',
		'6th',
		'7th',
		'8th',
		'9th',
		'10th',
		'11th',
		'12th',
		'13th',
		'14th',
		'15th',
		'16th',
		'17th',
		'18th',
		'19th',
		'20th',
		'21st',
		'22nd',
		'23rd',
		'24th',
		'25th',
		'26th',
		'27th',
		'28th',
		'29th',
		'30th',
	];
	icsiForm: FormGroup;
	visitsForm: FormGroup;
	ObstetricHistorySpecificForm: FormGroup;
	publishForm: FormGroup;
	patientAge;
	modalRef: NgbModalRef;
	partnerAge;
	labResults = [
		{
			amh: '',
			fsh: '',
			lh: '',
			e2: '',
			tsh: '',
			prolactin: '',
			rgbs: '',
			hba1c: '',

		},
	];

	semenParameters = [
		{
			countMill: '',
			a: '',
			b: '',
			viable: '',
			normal: '',
		}
	];
	radioChecked: Boolean = false;
	id = this.route.snapshot.paramMap.get('id');
	editing: any = {};
	constructor(
		private readonly modalService: NgbModal,
		private readonly route: ActivatedRoute,
		private readonly patientService: PatientService,
		private readonly _snackBar: MatSnackBar,
		private readonly fb: FormBuilder
	) {}

	ngOnInit(): void {
		this.initObstetricHistorySpecific();
		this.initIcsiForm();
		this.initVisitForm();
		this.initGuideMap();
		if (this.id && this.id !== '') {
			this.patientService.getPatients(this.id).subscribe(
				(res: res) => {
					this.patient = res.sentObject;

					if (this.patient.birthDate) {
						this.patient.birthDate = formatDate(
							this.patient.birthDate,
							'yyyy-MM-dd',
							'en'
						);
					}
					if (this.patient.lastVisit) {
						this.patient.lastVisit = formatDate(
							this.patient.lastVisit,
							'yyyy-MM-dd',
							'en'
						);
					}
					if (this.patient.marriageDate) {
						this.patient.marriageDate = formatDate(
							this.patient.marriageDate,
							'yyyy-MM-dd',
							'en'
						);
					}

					if (this.patient.icsiRecords.partnersBirthDate) {
						this.patient.icsiRecords.partnersBirthDate = formatDate(
							this.patient.icsiRecords.partnersBirthDate,
							'yyyy-MM-dd',
							'en'
						);
						this.partnerAge = this.calculateAge(
							this.patient.icsiRecords.partnersBirthDate
						);
					}

					this.icsiForm.patchValue(this.patient);
					this.icsiForm.patchValue(this.patient.icsiRecords);
					this.patientAge = this.calculateAge(this.patient.birthDate);

					if (this.icsiForm.get('previousICSIIUI').value) {
						this.radioChecked = true;
					}
					this.labResults = [this.patient.icsiRecords.laboratoryResults];
					this.semenParameters = [this.patient.icsiRecords.semenParameters];
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);
		}

		this.initPublishForm();
	}

	initGuideMap() {
		const template = `E2 ˂ 3500 pm/l ||

    P ratio ˂1.5 ||
    (p×1000/E2) ||
    U/S ET >8mm  ||  ,  || triple line follicles 18-20 mm >3
    5,6,10,11`;
		this.guideMap.set('COC', '10,11');
		this.guideMap.set('GNRHa', '10,11');
		this.guideMap.set(
			'Gonadotrophins',
			`E2(˂50) ||
    LH(˂5) ||
    FSH(˂5) ||
    NO Ovarian Cyst >3cm ||
    Endometrial  thickness >6mm ||
    2,3,4,7,9,10,11 `
		);
		this.guideMap.set('GNRHan', '1,8,9,10,11');
		this.guideMap.set('Folliculometry', '1,8,9,10,11');
		this.guideMap.set('Trigger', template);
	}

	dynamicWidth(event) {
		const HTMLNode = event.target;
		const value = event.target.value;
		HTMLNode.style.width = `calc(${value.length}ch + 35px)`;
	}

	calculateAge(date) {
		const today = new Date();
		const birthDate = new Date(date);
		let age = today.getFullYear() - birthDate.getFullYear();
		const m = today.getMonth() - birthDate.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--;
		}
		return age;
	}

	showForm() {}

	initPublishForm() {
		this.publishForm = this.fb.group({
			title: [{ value: 'ICSI File', disabled: true }],
			published: [false],
		});
	}

	initIcsiForm() {
		this.icsiForm = this.fb.group({
			code: ['', Validators.required],
			name: ['', Validators.required],
			birthDate: ['', Validators.required],
			visitNumber: [''],
			lastVisit: [''],
			martialStatus: ['', Validators.required],
			marriageDate: ['', Validators.required],
			partnersBirthDate: ['', Validators.required],
			occupation: ['', Validators.required],
			partnersOccupation: ['', Validators.required],
			previousICSIIUI: ['', Validators.required],
			details: ['', Validators.required],
			hysteroscopyConclusion: ['', Validators.required],
			threeDUSConclusion: ['', Validators.required],
			category: ['', Validators.required],
			protocol: ['', Validators.required],
			opType: ['', Validators.required],
			duration: ['', Validators.required],
			nOfEggsM1: ['', Validators.required],
			m2: ['', Validators.required],
			outCome: ['', Validators.required],
			gonadotrophins: [''],
			c4: [''],
			c8: [''],
			obstetricHistory: this.fb.group({
				g: [''],
				living: [''],
				male: [''],
				female: [''],
				p: this.fb.group({
					first: [''],
					second: [''],
					third: [''],
				}),
				anaesthesiaNote: [''],
				operativeNote: [''],
				complicationOccurred: [''],
				babyWeight: [''],
				hg: [''],
				bp: [''],
			}),
		});
	}

	initObstetricHistorySpecific() {
		this.ObstetricHistorySpecificForm = this.fb.group({
			rank: [''],
			w: [''],
			number: [''],
			modeOfTermination: [''],
			year: [],
			anaesthesiaNote: [''],
			operativeNote: [''],
			complicationOccurred: [''],
			babyWeight: [''],
			hg: this.fb.group({
				before: [''],
				after: [''],
			}),
			bp: this.fb.group({
				before: [''],
				after: [''],
			}),
		});
	}

	initVisitForm() {
		this.visitsForm = this.fb.group({
			visit: ['', Validators.required],
			title: ['', Validators.required],
			date: ['', Validators.required],
			lmp: ['', Validators.required],
			e2: ['', Validators.required],
			fsh: ['', Validators.required],
			lh: ['', Validators.required],
			p: ['', Validators.required],
			pRatio: ['', Validators.required],
			afc: ['', Validators.required],
			rtOvary: ['', Validators.required],
			ltOvary: ['', Validators.required],
			et: ['', Validators.required],
			drug: ['', Validators.required],
			nextVisit: ['', Validators.required],
		});
	}


	AddObstetricHistory() {
		this.patientService
			.AddObstetricHistoryIcsi(
				this.id,
				this.ObstetricHistorySpecificForm.value
			)
			.subscribe((res: res) => {
				this.patient.icsiRecords.obstetricHistory.specific = [
					...res.sentObject,
				];
        this.modalRef.close()
			});
	}

	updateValue(event: any, cell: string, rowIndex: number) {
		const value = event.target.value;
		const oldValue = this.patient.icsiRecords.visits[rowIndex][cell];
		const id = this.patient.icsiRecords.visits[rowIndex]._id;
		if (oldValue === value)
			return (this.editing[rowIndex + '-' + cell] = false);

		this.patientService
			.updateIcsiVisit(this.id, id, {
				[cell]: value,
			})
			.subscribe(
				(res) => {
					this.patient.icsiRecords.visits[rowIndex][cell] = value;
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);

		this.editing[rowIndex + '-' + cell] = false;
	}

	updateLabRes(event: any, cell: string, rowIndex: number) {
		const value = event.target.value;
		const oldValue = this.patient.icsiRecords.laboratoryResults[cell];
		// const id = this.patient.icsiRecords.laboratoryResults;
		if (oldValue === value)
			return (this.editing[rowIndex + '-' + cell] = false);

		this.patientService
			.updateLab(this.id, {
				[cell]: value,
			})
			.subscribe(
				(res) => {
					this.patient.icsiRecords.laboratoryResults[cell] = value;
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);

		this.editing[rowIndex + '-' + cell] = false;
	}


  updateSemenParameters(event: any, cell: string, rowIndex: number) {
		const value = event.target.value;
		const oldValue = this.patient.icsiRecords.semenParameters[cell];
		// const id = this.patient.icsiRecords.semenParameters;
		if (oldValue === value)
			return (this.editing[rowIndex + '-' + cell] = false);

		this.patientService
			.updateSemenParameter(this.id, {
				[cell]: value,
			})
			.subscribe(
				(res) => {
					this.patient.icsiRecords.semenParameters[cell] = value;
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);

		this.editing[rowIndex + '-' + cell] = false;
	}

	// updateMobility(a, b, cell, rowIndex) {
	// 	this.patientService
	// 		.updateLab(this.id, {
	// 			[cell]: { a, b },
	// 		})
	// 		.subscribe(
	// 			(res) => {
	// 				this.patient.icsiRecords.laboratoryResults[cell].a = a;
	// 				this.patient.icsiRecords.laboratoryResults[cell].b = b;
	// 			},
	// 			(err) => this._snackBar.open(err.error.message, 'close')
	// 		);
	// 	this.editing[rowIndex + '-' + cell] = false;
	// }

	updateObstetricHistory(event, c, index) {
		const value = event.target.value;
		const cell = c.split('.');
		const id = this.patient.records.obstetricHistory.specific[index]._id;
		this.patientService
			.updateObstetricHistoryIcsi(this.id, id, {
				[c]: value,
			})
			.subscribe((res: res) => {
				if (cell.length > 1) {
					this.patient.icsiRecords.obstetricHistory.specific[index][
						cell[0]
					][cell[1]] = value;
				} else {
					this.patient.icsiRecords.obstetricHistory.specific[index][
						cell[0]
					] = value;
				}
			});
	}

	deleteObstetricHistory(id) {
		this.patientService
			.deleteObstetricHistoryIcsi(this.id, id)
			.subscribe((res: res) => {
				this.patient.icsiRecords.obstetricHistory.specific = [
					...res.sentObject,
				];
			});
	}

	updateICSIFile() {
		this.patientService.updateICSI(this.id, this.icsiForm.value).subscribe(
			(res: res) => {
				this._snackBar.open(res.message, 'close');
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}

	createVisit() {
		this.patientService
			.addICSIVisits(this.id, this.visitsForm.value)
			.subscribe(
				(res: res) => {
					const id = res.sentObject;
					this.patient.icsiRecords.visits = [
						...this.patient.icsiRecords.visits,
						{ id, ...this.visitsForm.value },
					];
					this.modalRef.close();
				},
				(err) => this._snackBar.open(err.error.message, 'close')
			);
	}

	openModal(modal: any) {
		this.modalRef = this.modalService.open(modal, {
			size: 'xl',
			centered: true,
		});
		this.modalRef.result.then(
			(result) => {
				this.visitsForm.reset();

				this.publishForm.reset();
			},
			(reason) => {
				this.visitsForm.reset();

				this.publishForm.reset();
			}
		);
	}

	showAge(event: Event, partAge: HTMLInputElement) {
		const value = (<HTMLDataElement>event.target).value;
		const age = this.calculateAge(value);
		partAge.value = age.toString();
	}

	deleteVisit(id, i) {
		this.patientService.deleteIcsiVisit(this.id, id).subscribe(
			(res) => {
				this.patient.icsiRecords.visits.filter((v, index) => {
					return index !== i;
				});
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}

	publishIcsi() {
		const body = {title:'ICSI File',...this.publishForm.value};
		this.patientService.publishIcsi(this.id, body).subscribe(
			(res) => {
				this.modalRef.close();
			},
			(err) => this._snackBar.open(err.error.message, 'close')
		);
	}
}
