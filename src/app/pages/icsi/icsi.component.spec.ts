import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IcsiComponent } from './icsi.component';

describe('IcsiComponent', () => {
  let component: IcsiComponent;
  let fixture: ComponentFixture<IcsiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IcsiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IcsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
