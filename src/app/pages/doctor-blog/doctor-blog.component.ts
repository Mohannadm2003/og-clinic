import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { postType } from 'src/app/Shared/enum/types.enum';
import { PostService } from 'src/app/Shared/services/post/post.service';

import { res } from '../../Shared/models/res.model';

@Component({
  selector: 'app-doctor-blog',
  templateUrl: './doctor-blog.component.html',
  styleUrls: ['./doctor-blog.component.css'],
})
export class DoctorBlogComponent implements OnInit {
  posts: any[];
  pager = {
    pageSize: 0,
    length: 0,
    page: 0,
  };
  queryParams;
  isLoading = true;
  cat;
  constructor(
    private readonly postService: PostService,
    private readonly _snackBar: MatSnackBar,
    private readonly route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.cat = this.route.snapshot.queryParamMap.get('cat');
    this.route.queryParams.subscribe((p) => {
      this.queryParams = JSON.parse(JSON.stringify(p));
      this.pager.page = p.page;
    });
    this.postService
      .getPost('', 12, this.pager.page, postType.drug, this.cat)
      .subscribe(
        (res: res) => {
          this.pager.pageSize = 12;
          this.pager.length = res.count;
          this.posts = res.sentObject;
          this.isLoading = false;
        },
        (err) => {
          this._snackBar.open(err.error.message, 'close');
          this.isLoading = false;
        }
      );
  }

  handlePage(event: PageEvent) {
    this.postService
      .getPost('', 12, event.pageIndex + 1, postType.drug, this.cat)
      .subscribe(
        (res: res) => {
          this.pager.pageSize = 12;
          this.pager.page = event.pageIndex + 1;
          this.queryParams.page = event.pageIndex + 1;
          this.pager.length = res.count;
          this.posts = res.sentObject;
          this.isLoading = false;
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: this.queryParams,
            queryParamsHandling: 'merge', // remove to replace all query params by provided
          });
          // this.posts.forEach((p) => {
          //   p.img = this.sanitizer.bypassSecurityTrustUrl(
          //     'http://localhost:3000' + p.img
          //   );
          // });
        },
        (err) => {
          this._snackBar.open(err.error.message, 'close');
          this.isLoading = false;
        }
      );
  }
}
