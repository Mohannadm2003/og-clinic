import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DoctorService } from 'src/app/Shared/services/doctor/doctor.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  forgetPassword: FormGroup;
  isSent;
  constructor(private fb: FormBuilder, private docterService: DoctorService, private _sb: MatSnackBar) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.forgetPassword = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    })
  }

  login() {
    this.docterService.SendLoginToken(this.forgetPassword.value).subscribe((res) => {this.isSent = true }, (err: any) => this._sb.open(err.error.message, 'close'));
  }
}
