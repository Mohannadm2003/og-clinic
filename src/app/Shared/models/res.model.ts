import { Post } from './post.model';

export interface res {
  message: string;
  sentObject: any[] | any;
  count?: number;
  pages?: number;
}
