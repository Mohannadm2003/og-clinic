import { SafeHtml, SafeUrl } from '@angular/platform-browser';

export interface Post {
  title: string;
  body: string | SafeHtml;
  category: string;
  drugForms: [
    { _id: string; index: number; form: string; product: string; value: string }
  ];
  published: boolean;
  drugType;
  img: string | SafeUrl;
  createdAt: Date;
  updatedAt: Date;
  myPrescription:{
    doctorId;
    value;
    postId;
    _id;
  };
}
