import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { catchError, first } from 'rxjs/operators';
import { Subject, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root',
})
export class DoctorService {
  constructor(
    private http: HttpClient,
    private route: Router,
    private cookieService: CookieService
  ) {}
  // apiUrl = 'http://localhost:3000/';
  // apiUrl = 'http://qaym.one';

  private tokenTimer;
  isAuth: Boolean = false;
  isAuthlistener: Subject<boolean> = new Subject();

  isEnabled() {
    return this.cookieService.get('enabled');
  }

  isMyClinic() {
    return this.cookieService.get('inMyClinic');
  }

  getMedicalStorage() {
    return this.http.get('/api/' + 'doctors/doctor/storage/').pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  getMedicalNetwork() {
    return this.http.get('/api/' + 'doctors/doctor/network/').pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  addToCalendar(id, body) {
    return this.http.post('/api/' + 'doctors/doctor/calendar/' + id, body).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  getCalendarEvents() {
    return this.http.get('/api/' + 'doctors/doctor/calendar').pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  deleteCalendarEvent(id) {
    return this.http.delete('/api/' + 'doctors/doctor/calendar/' + id).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  updateCalendar(id, body) {
    return this.http.put('/api/' + 'doctors/doctor/calendar/' + id, body).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  getCountries() {
    return this.http.get('/api/countries').pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  getCitiesOFCountry(code) {
    return this.http.get('/api/cites?' + `code=${code}`).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }
  signUp(doctor: any) {
    return this.http.post('/api/' + 'doctors/doctor/signup', doctor, {}).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }
  logIn(doctor: any) {
    return this.http.post('/api/' + 'doctors/doctor/login', doctor).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  myClinicLogIn(doctor: any) {
    return this.http
      .post('/api/' + 'doctors/doctor/mycliniclogin', doctor)
      .pipe(
        first(),
        catchError((err) => throwError(err))
      );
  }
  getDoctors(pageSize?: number, currentPage?: number) {
    return this.http
      .get(
        '/api/' +
          'doctors' +
          `?pageSize=${pageSize}&&currentPage=${currentPage}`
      )
      .pipe(
        first(),
        catchError((err) => throwError(err))
      );
  }
  getDoctor(id) {
    return this.http.get('/api/' + 'doctors/doctor/' + id).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }
  updateDoctor(id: string, doctor: any) {
    return this.http.put('/api/' + 'doctors/doctor/' + id, doctor).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  updatePassword(id: string, doctor: any) {
    return this.http
      .put('/api/' + 'doctors/doctor/password/' + id, doctor)
      .pipe(
        first(),
        catchError((err) => throwError(err))
      );
  }
  deleteDoctor(id: string) {
    return this.http.delete('/api/' + 'doctors/doctor/' + id).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  addToMedicalStorage(body) {
    return this.http.post('/api/' + '/doctors/doctor/storage/', body).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  updateMedicalStorage(id, body) {
    return this.http
      .put(
        '/api/' + '/doctors/doctor/storage/' + `?medicalSupplyId=${id}`,
        body
      )
      .pipe(
        first(),
        catchError((err) => throwError(err))
      );
  }

  deleteMedicalStorage(id) {
    return this.http
      .delete('/api/' + '/doctors/doctor/storage/' + `?medicalSupplyId=${id}`)
      .pipe(
        first(),
        catchError((err) => throwError(err))
      );
  }

  addMedicalNetwork(body) {
    return this.http.post('/api/' + '/doctors/doctor/network/', body).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  updateMedicalNetwork(id, body) {
    return this.http
      .put(
        '/api/' + '/doctors/doctor/network/' + `?medicalNetworkId=${id}`,
        body
      )
      .pipe(
        first(),
        catchError((err) => throwError(err))
      );
  }

  updateDoctorTools(id, body) {
    return this.http.put('/api/' + '/doctors/doctor/enable/' + id, body).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  updateDoctorClinic(id, body) {
    return this.http.put('/api/' + '/doctors/doctor/myclinic/' + id, body).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  deleteMedicalNetwork(id) {
    return this.http
      .delete('/api/' + '/doctors/doctor/network/' + `?medicalNetworkId=${id}`)
      .pipe(
        first(),
        catchError((err) => throwError(err))
      );
  }

  filterDoctor(country = '', city = '', name = '') {
    return this.http
      .get('/api/' + '/doctors/doctor/search', {
        params: {
          country,
          city,
          name,
        },
      })
      .pipe(
        first(),
        catchError((err) => throwError(err))
      );
  }

  getAllDoctors(pageSize: number = 20, currentPage: number =1 ) {
    return this.http
      .get('/api/' + 'doctors/ad', {
        params: {
          pageSize,
          currentPage,
        },
      })
      .pipe(
        first(),
        catchError((err) => throwError(err))
      );
  }

  updateSubsExpDate(id, body) {
    return this.http.put('/api/' + 'doctors/doctor/subs/' + id, body).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  logOut() {
    this.isAuth = false;
    this.cookieService.deleteAll();
    this.isAuthlistener.next(false);
    clearTimeout(this.tokenTimer);
    this.route.navigate(['/system/login']);
  }

  setAuthTimer(duration) {
    setTimeout(() => {
      this.logOut();
    }, duration * 1000);
  }

  getisAuthStatus() {
    return this.isAuth;
  }
  getisAuthStatusListener() {
    return this.isAuthlistener.asObservable();
  }

  getUserId() {
    return this.cookieService.get('userId');
  }
  getToken() {
    return this.cookieService.get('token');
  }

  saveAuthData(token: string, UserDoc: any, role, enabled, inMyClinic) {
    const now = new Date();
    const expirationDate = new Date(now.getTime() + 9 * 3600 * 1000);
    this.cookieService.set('token', token, {
      expires: expirationDate,
      secure: true,
    });
    this.cookieService.set('expirationDate', expirationDate.toISOString());
    this.cookieService.set('enabled', enabled, {
      expires: expirationDate,
      secure: true,
    });
    this.cookieService.set('role', role, {
      expires: expirationDate,
      secure: true,
    });
    this.cookieService.set('inMyClinic', inMyClinic, {
      expires: expirationDate,
      secure: true,
    });
    // delete UserDoc.calendar;
    // delete UserDoc
    this.cookieService.set('userId', UserDoc._id, {
      expires: expirationDate,
    });
    this.cookieService.set('userRole', UserDoc.type, {
      expires: expirationDate,
    });
    this.cookieService.set('userImg', UserDoc.img, {
      expires: expirationDate,
    });
  }

  private getAuthData() {
    const token = this.cookieService.get('token');
    const expirationDate = this.cookieService.get('expirationDate');
    const userId = this.cookieService.get('userId');
    const userRole = this.cookieService.get('userRole');
    const userImg = this.cookieService.get('userRole');

    if (!token && !expirationDate && !userId) {
      return;
    }
    return {
      token,
      expirationDate: new Date(expirationDate),
      // userDoc: JSON.parse(userDocJson),
    };
  }
  autoAuthUser() {
    const authInformation = this.getAuthData();
    if (Object.keys(authInformation).length === 0) {
      return;
    }
    this.isAuth = true;
    const now = new Date();
    const expiresIN = authInformation.expirationDate.getTime() - now.getTime();
    if (expiresIN > 0) {
      this.isAuth = true;
      this.setAuthTimer(expiresIN / 1000);
      this.isAuthlistener.next(true);
    }
  }

  SendLoginToken(email) {
    return this.http.post('/api/' + 'doctors/doctor/resetPassword', email).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }

  resetPassword(body) {
    return this.http.put('/api/' + 'doctors/doctor/resetPassword', body).pipe(
      first(),
      catchError((err) => throwError(err))
    );
  }
}
