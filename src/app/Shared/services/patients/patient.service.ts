import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, first, switchMap } from 'rxjs/operators';
import { throwError } from 'rxjs';
@Injectable({
	providedIn: 'root',
})
export class PatientService {
	constructor(private http: HttpClient) {}
	// apiUrl = 'http://localhost:3000/';
	// apiUrl = 'http://qaym.one';

	search(code: string = '', name: string = '') {
		return this.http
			.get('/api/' + 'patients/search', {
				params: {
					code,
					name,
				},
			})
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	getPatients(
		id: string = '',
		pageSize: number | string = '',
		currentPage: number | string = '',
		code: String = ''
	) {
		if (id) {
			return this.http.get('/api/' + 'patients/patient/' + id).pipe(
				first(),
				catchError((err) => throwError(err))
			);
		} else if (code.trim() !== '') {
			return this.http.get('/api/' + 'patients/' + `?code=${code}`).pipe(
				first(),
				catchError((err) => throwError(err))
			);
		} else {
			return this.http
				.get(
					'/api/' +
						'patients/' +
						`?pageSize=${pageSize}&&currentPage=${currentPage}`
				)
				.pipe(
					first(),
					catchError((err) => throwError(err))
				);
		}
	}

	addObstetricHistory(id, body) {
		return this.http
			.post('/api/' + 'patients/patient/record/obstetricHistory/' + id, body)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	DeleteObstetricHistory(id, obstetricId) {
		return this.http
			.delete('/api/' + 'patients/patient/record/obstetricHistory/' + id, {
				params: {
					obstetricId,
				},
			})
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}
	deleteObstetricHistoryIcsi(id, obstetricId) {
		return this.http
			.delete('/api/' + 'patients/patient/icsi/obstetricHistory/' + id, {
				params: {
					obstetricId,
				},
			})
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	createPatient(patient: any) {
		return this.http.post('/api/' + 'patients', patient).pipe(
			first(),
			catchError((err) => throwError(err))
		);
	}

	getRecord(id, title, type) {
		return this.http
			.get('/api/' + 'patients/records/' + id, {
				params: {
					title,
					type,
				},
			})
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	publishRecord(id, body) {
		return this.http
			.put('/api/' + 'patients/patient/record/publish/' + id, body)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	publishIcsi(id, body) {
		return this.http
			.put('/api/' + 'patients/patient/icsi/publish/' + id, body)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	updatePatient(id: string, patient: any) {
		return this.http.put('/api/' + 'patients/patient/' + id, patient).pipe(
			first(),
			catchError((err) => throwError(err))
		);
	}

	updateObstetricHistory(id, obstetricId, body) {
		return this.http
			.put('/api/' + 'patients/patient/obstetric/property/' + id, body, {
				params: {
					obstetricId,
				},
			})
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	updateObstetricHistoryIcsi(id, obstetricId, body) {
		return this.http
			.put('/api/' + 'patients/patient/icsiobstetric/property/' + id, body, {
				params: {
					obstetricId,
				},
			})
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	updateICSI(id, patient) {
		return this.http.put('/api/' + 'patients/icsi/' + id, patient).pipe(
			first(),
			catchError((err) => throwError(err))
		);
	}

	updateLab(id, lab) {
		return this.http
			.put('/api/' + 'patients/patient/labres/property/' + id, lab)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	updateSemenParameter(id, lab) {
		return this.http
			.put('/api/' + 'patients/patient/semen/property/' + id, lab)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	addICSIVisits(id, visit) {
		return this.http.post('/api/' + 'patients/icsivisit/' + id, visit).pipe(
			first(),
			catchError((err) => throwError(err))
		);
	}

	deletePatient(id: string) {
		return this.http.delete('/api/' + 'patients/patient/' + id).pipe(
			first(),
			catchError((err) => throwError(err))
		);
	}

	searchPatient(code) {
		return this.http.get('/api/' + 'patients/search' + `?code=${code}`).pipe(
			first(),
			catchError((err) => throwError(err))
		);
	}

	addVisit(patientId, visit) {
		return this.http
			.post('/api/' + 'patients/patient/visit/' + patientId, visit)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}
	deleteVisit(patientId, visitId) {
		return this.http
			.delete(
				'/api/' +
					'patients/patient/visit/' +
					patientId +
					`?visitId=${visitId}`
			)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	deleteIcsiVisit(patientId, visitId) {
		return this.http
			.delete(
				'/api/' +
					'patients/patient/icsivisit/' +
					patientId +
					`?visitId=${visitId}`
			)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	updateVisits(patientId, visitId, visit) {
		return this.http
			.put(
				'/api/' +
					'patients/patient/visit/property/' +
					patientId +
					`?visitId=${visitId}`,
				visit
			)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}
	addContraceptiveHistory(patientId, contraceptive) {
		return this.http
			.post(
				'/api/' + 'patients/patient/contraceptive/' + patientId,
				contraceptive
			)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	updateContraceptiveHistory(patientId, contraceptiveId, contraceptive) {
		return this.http
			.put(
				'/api/' +
					'patients/patient/contraceptive/property/' +
					patientId +
					`?contraceptiveId=${contraceptiveId}`,
				contraceptive
			)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	deleteContraceptiveHistory(patientId, contraceptiveId) {
		return this.http
			.delete(
				'/api/' +
					'patients/patient/contraceptive/' +
					patientId +
					`?contraceptiveId=${contraceptiveId}`
			)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	AddObstetricHistoryIcsi(patientId, obtetric) {
		return this.http
			.post(
				'/api/' + 'patients/patient/icsi/obstetricHistory/' + patientId,
				obtetric
			)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	addSurgery(patientId, surgery) {
		return this.http
			.post('/api/' + 'patients/patient/surgery/' + patientId, surgery)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	updateSurgery(patientId, surgeryId, surgery) {
		return this.http
			.put(
				'/api/' +
					'patients/patient/surgery/property/' +
					patientId +
					`?surgeryId=${surgeryId}`,
				surgery
			)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	deleteSurgery(patientId, surgeryId) {
		return this.http
			.delete(
				'/api/' +
					'patients/patient/surgery/' +
					patientId +
					`?surgeryId=${surgeryId}`
			)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	updateIcsiVisit(patientId, visitId, Visit) {
		return this.http.put(
			'/api/' +
				'patients/patient/icsivisit/property/' +
				patientId +
				`?visitId=${visitId}`,
			Visit
		);
	}
}
