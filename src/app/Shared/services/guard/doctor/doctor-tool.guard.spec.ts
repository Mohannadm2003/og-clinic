import { TestBed } from '@angular/core/testing';

import { DoctorToolGuard } from './doctor-tool.guard';

describe('DoctorToolGuard', () => {
  let guard: DoctorToolGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(DoctorToolGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
