import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, first } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { postType } from '../../enum/types.enum';

@Injectable({
	providedIn: 'root',
})
export class PostService {
	constructor(private http: HttpClient) { }
	// apiUrl = 'http://localhost:3000/';
	// apiUrl = 'http://qaym.one';

	searchInRecords(s) {
		return this.http
			.get('/api/' + 'patients/patient/records/search', { params: { s } })
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	publishUnPublishPost(id, body) {
		return this.http.put('/api/' + 'posts/post/publish/' + id, body).pipe(
			first(),
			catchError((err) => throwError(err))
		);
	}

	createPost(Post: any) {
		return this.http.post('/api/' + 'posts/post', Post).pipe(
			first(),
			catchError((err) => throwError(err))
		);
	}
	getPost(
		id: string = '',
		pageSize?: number,
		currentPage?: number,
		type?: string,
		cat?: string
	) {
		if (pageSize && currentPage && type !== postType.drug) {
			return this.http
				.get('/api/' + 'posts/', {
					params: {
						pageSize,
						currentPage,
						type,
						cat: cat ? cat : '',
					},
				})
				.pipe(
					first(),
					catchError((err) => throwError(err))
				);
		} else if (pageSize && currentPage && type === postType.drug) {
			return this.http
				.get('/api/' + 'posts/tools', {
					params: {
						pageSize,
						currentPage,
						type,
						cat,
					},
				})
				.pipe(
					first(),
					catchError((err) => throwError(err))
				);
		}
		return this.http.get('/api/' + 'posts/post/' + id).pipe(
			first(),
			catchError((err) => throwError(err))
		);
	}
	updatePost(id: string, Post: any) {
		return this.http.put('/api/' + 'posts/post/' + id, Post).pipe(
			first(),
			catchError((err) => throwError(err))
		);
	}

	deletePost(id: string = '') {
		return this.http.delete('/api/' + 'posts/post/' + id).pipe(
			first(),
			catchError((err) => throwError(err))
		);
	}

	getToolsPost(id) {
		return this.http.get('/api/' + 'posts/tools/' + id).pipe(
			first(),
			catchError((err) => throwError(err))
		);
	}

	addPrescriptions(id, body) {
		return this.http
			.post(
				'/api/' + 'posts/tools/prescripton/' + id + '?formId=' + id,
				body
			)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	updatePrescriptions(id, body) {
		return this.http
			.put('/api/' + 'posts/tools/prescripton/' + id, body)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	getAdminPost(pageSize: number, currentPage: number) {
		return this.http
			.get(
				'/api/' +
				'posts/post/admin' +
				`?pageSize=${pageSize}&&currentPage=${currentPage}`
			)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	getRecords(pageSize?: number, currentPage?: number) {
		return this.http
			.get(
				'/api/' +
				'patients/records' +
				`?pageSize=${pageSize}&&currentPage=${currentPage}`
			)
			.pipe(
				first(),
				catchError((err) => throwError(err))
			);
	}

	deleteImages(name) {
		return this.http.delete('/api/' + 'posts/post/deleteImg', { params: { imgName: name } })
	}
}
