import { Component, OnInit } from '@angular/core';
import {
	faFacebookSquare,
	faWhatsappSquare,
	faYoutubeSquare,
} from '@fortawesome/free-brands-svg-icons';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import { faMapMarker, faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.css'],
})
export class FooterComponent implements OnInit {
	faEnvelope = faEnvelope;
	faMapMarker = faMapMarker;
	faPhoneAlt = faPhoneAlt;
	faWhatsappSquare = faWhatsappSquare;
	faFacebookSquare = faFacebookSquare;
	faYoutubeSquare = faYoutubeSquare;
	constructor() {}

	ngOnInit(): void {}
}
