import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { DoctorService } from '../../services/doctor/doctor.service';
import {
  faStickyNote,
  faEdit,
  faCalendar,
} from '@fortawesome/free-regular-svg-icons';
import {
  faSignOutAlt,
  faGlobe,
  faCubes,
  faUserMd,
  faUsers,
  faHome,
  faToolbox,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css'],
})
export class ToolbarComponent implements OnInit {
  id;
  role;
  imglink;
  faUsers = faUsers;
  faToolbox = faToolbox;
  faHome = faHome;
  faUserMd = faUserMd;
  faStickyNote = faStickyNote;
  faEdit = faEdit;
  faCalendar = faCalendar;
  faSignOutAlt = faSignOutAlt;
  faGlobe = faGlobe;
  faCubes = faCubes;
  constructor(
    private cookieService: CookieService,
    private doctorService: DoctorService
  ) {}

  ngOnInit(): void {
    // const userDoc = JSON.parse(this.cookieService.get('userDoc'));

    this.id = this.cookieService.get('userId');
    this.role = this.cookieService.get('userRole');
    this.imglink = this.cookieService.get('userImg');
  }

  logout() {
    this.doctorService.logOut();
  }
}
