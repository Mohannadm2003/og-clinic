import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {
  Component,
  ViewChild,
  TemplateRef,
  OnInit,
  ChangeDetectorRef,
  ViewEncapsulation,
  ChangeDetectionStrategy,
} from '@angular/core';
import {
  startOfDay,
  isSameDay,
  isSameMonth,
  addMinutes,
  isAfter,
} from 'date-fns';

import { Subject } from 'rxjs';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';
import { DoctorService } from '../../services/doctor/doctor.service';
import { res } from '../../models/res.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { formatDate } from '@angular/common';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarComponent implements OnInit {
  activeDayIsOpen;

  eventFixedDuration = 60; // minutes
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;
  @ViewChild('template', { static: true }) temp: TemplateRef<any>;

  eventForm: FormGroup;
  view: CalendarView = CalendarView.Month;
  isEditing = false;
  viewDate: Date = new Date();
  modalRef: NgbModalRef;
  modalData: {
    action: string;
    event: CalendarEvent;
  };

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] | any = [];
  CalendarView = CalendarView;
  selectedEvent;
  actions: CalendarEventAction[] = [
    {
      label: `<span>Edit &nbsp</span>`,
      onClick: ({ event }: { event: any }): void => {
        event['state'] = 1;
        event.color = colors.blue;
        event.start = formatDate(event.start, 'yyyy-MM-dd', 'en');
        event.end = formatDate(event.end, 'yyyy-MM-dd', 'en');
        this.isEditing = true;
        this.selectedEvent = event;
        this.eventForm.patchValue(event);
        this.modalRef = this.modalService.open(this.temp, {
          centered: true,
          size: 'xl',
        });
        // this.handleEvent('Edited', event);
      },
    },
    {
      label: '<span>Delete</span>',
      onClick: ({ event }: { event: any }) => {
        // event['state'] = -1;
        // const index = this.events.findIndex(
        // 	(iEvent) => iEvent._id === event._id
        // );
        // if (index > -1) {
        // 	this.events.splice(index, 1);
        // }
        this.doctorService
          .deleteCalendarEvent(event._id)
          .subscribe((res: res) => {
            this.events = [
              ...this.events.filter((iEvent) => event._id !== iEvent._id),
            ];
          });
        // this.handleEvent('Deleted', event);
        // this.allToProcessEvents.push(event);
      },
    },
  ];

  id: any = this.doctorService.getUserId();

  constructor(
    private modalService: NgbModal,
    private readonly doctorService: DoctorService,
    private readonly _snackBar: MatSnackBar,
    private readonly fb: FormBuilder,
    private cdr: ChangeDetectorRef
  ) {
    this.doctorService.getCalendarEvents().subscribe(
      (res: res) => {
        res.sentObject.forEach((element) => {
          element.start = new Date(element.start);
          element.end = new Date(element.end);
          element.actions = this.actions;
        });
        this.events = [...res.sentObject];
        this.cdr.detectChanges();
      },
      (err) => this._snackBar.open(err.error.message, 'close')
    );
  }

  ngOnInit(): void {
    this.initEditForm();
  }

  changeFun() {}

  initEditForm() {
    this.eventForm = this.fb.group(
      {
        title: [''],
        start: [''],
        end: [''],
      },
      { validators: this.validateDate }
    );
  }

  validateDate(control: AbstractControl) {
    const startDate = new Date(control.get('start').value);
    const endDate = new Date(control.get('end').value);

    const isTrue = isAfter(endDate, startDate);
    console.log(isTrue);
    return isTrue ? null : { isAfter: false };
  }

  openModal(content: any) {
    this.modalService.open(content, { centered: true, size: 'xl' });
  }
  close() {
    this.modalService.dismissAll();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    if (action === 'Dropped or resized' || action === 'Edited') {
      // this.updateEvent(event);
    }
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
      this.cdr.detectChanges();
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    if (event['state'] !== 0) {
      event['state'] = 1;
      event.color = colors.blue;
    }
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    const copy = Object.assign({}, event);
    copy.start = newStart;
    copy.end = newEnd;
  }

  addUpdateEvent() {
    const values = this.eventForm.value;
    const event: any = {
      title: values.title,
      start: startOfDay(new Date(values.start)),
      end: addMinutes(
        startOfDay(new Date(values.end)),
        this.eventFixedDuration
      ),
      color: colors.yellow,
      state: 0,
      actions: this.actions,
    };
    if (this.isEditing) {
      event._id = this.selectedEvent._id;
      event.color = colors.blue;
      this.doctorService
        .updateCalendar(this.selectedEvent._id, event)
        .subscribe((res: res) => {
          res.sentObject.forEach((element) => {
            element.start = new Date(element.start);
            element.end = new Date(element.end);
            element.actions = this.actions;
          });
          this.events = [...res.sentObject];
          console.log(this.events);
          this.isEditing = false;
          this.eventForm.reset();
          this.selectedEvent = null;
          this.cdr.detectChanges();
          this.close();
        });
    } else {
      this.doctorService.addToCalendar('', event).subscribe((res: res) => {
        res.sentObject.forEach((element) => {
          element.start = new Date(element.start);
          element.end = new Date(element.end);
          element.actions = this.actions;
        });
        console.log(this.events);

        this.events = [...res.sentObject];
        console.log(this.events);

        this.cdr.detectChanges();
        this.close();
      });
    }
  }
}
