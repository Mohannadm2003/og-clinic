import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './UI/navbar/navbar.component';
import { HeroComponent } from './UI/hero/hero.component';
import { FooterComponent } from './UI/footer/footer.component';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [NavbarComponent, HeroComponent, FooterComponent],
  imports: [CommonModule, RouterModule, FontAwesomeModule],
  exports: [NavbarComponent, HeroComponent, FooterComponent],
})
export class SharedUiModule {}
