import { TestBed } from '@angular/core/testing';

import { MyclinicGuard } from './myclinic.guard';

describe('MyclinicGuard', () => {
  let guard: MyclinicGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(MyclinicGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
