import { Pipe, PipeTransform } from '@angular/core';
import { differenceInCalendarWeeks } from 'date-fns/esm';

@Pipe({
  name: 'differenceinweeks'
})
export class DifferenceinweeksPipe implements PipeTransform {

  transform(value: string|Date|number, ...args: unknown[]): unknown {
    return differenceInCalendarWeeks(new Date(),new Date(value));
  }

}
