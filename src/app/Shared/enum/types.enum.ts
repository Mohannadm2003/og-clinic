export enum postType {
  patient = 'Patient',
  drug = 'Drug',
  nursing = 'Nursing',
}
