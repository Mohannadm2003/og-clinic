import { NgModule } from '@angular/core';
import { RecordsComponent } from '../pages/records/records.component';
import { RecordComponent } from '../pages/record/record.component';
import { NurseBlogComponent } from '../pages/nurse-blog/nurse-blog.component';
import { PatientBlogComponent } from '../pages/Patient-blog/Patien-blog.component';
import { PostComponent } from '../pages/post/post.component';
import { DoctorsCardsComponent } from '../pages/doctors-cards/doctors-cards.component';
import { HomeComponent } from '../pages/home/home.component';
import { RouterModule } from '@angular/router';
import { PatientHomeComponent } from '../pages/patient-home/patient-home.component';

const routes = [
	{ path: 'main', component: PatientHomeComponent },
	{ path: 'blog', component: PatientBlogComponent },
	{ path: 'nurse', component: NurseBlogComponent },
	{ path: 'records', component: RecordsComponent },
	{ path: 'records/:id', component: RecordComponent },
	{ path: 'blog/:id', component: PostComponent },
	{ path: 'doctors', component: DoctorsCardsComponent },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class PatientsRouterModule {}
