import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecordsComponent } from '../pages/records/records.component';
import { RecordComponent } from '../pages/record/record.component';
import { NurseBlogComponent } from '../pages/nurse-blog/nurse-blog.component';
import { PatientBlogComponent } from '../pages/Patient-blog/Patien-blog.component';
import { PostComponent } from '../pages/post/post.component';
import { DoctorsCardsComponent } from '../pages/doctors-cards/doctors-cards.component';
import { PatientHomeComponent } from '../pages/patient-home/patient-home.component';

// import { HeroComponent } from '../Shared/UI/hero/hero.component';

import { PatientsRouterModule } from './patients-router.module';
import { SharedModule } from '../Shared/shared-modules.module';
import { SharedUiModule } from '../Shared/shared-ui.module';

@NgModule({
  declarations: [
    PatientBlogComponent,
    DoctorsCardsComponent,
    NurseBlogComponent,
    RecordComponent,
    RecordsComponent,
    PostComponent,
    PatientHomeComponent,
  ],
  imports: [CommonModule, PatientsRouterModule, SharedModule, SharedUiModule],
})
export class PatientsModule {}
