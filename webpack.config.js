const path = require('path');
// var CompressionPlugin = require('compression-webpack-plugin');
const glob = require('glob');
const PurifyCSSPlugin = require('purifycss-webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");

module.export = {
  plugins: [
    new CompressionPlugin({
      algorithm:'gzip',
      compressionOptions: { level: 9 },
      deleteOriginalAssets: true,
    }),
    new ExtractTextPlugin('[name].[contenthash].css'),
    // Make sure this is after ExtractTextPlugin!
    new PurifyCSSPlugin({
      minimize:true,
      // Give paths to parse for rules. These should be absolute!
      paths: glob.sync(path.join(__dirname, 'dist/OG-clinic/*.html')),
    })
  ],
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },

};
